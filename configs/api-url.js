//const URL = 'https://cp83762.tmweb.ru/api/';
const URL = 'https://api2.trafficplace.org/api/';
const URLV = 'https://api.trafficplace.org/api/v1/';
//const URL = 'http://localhost:8000/api/';
//const URLV = 'http://localhost:7000/api/v1/';
//const URLV = 'https://traffic.masterholding.am/api/v1/';
export default {
    url: URL,
    urlv: URLV,
    register: `${URL}register`,
    login: `${URL}login`,
    logout: `${URLV}logout`,
    addCar: `${URLV}truck/create`,
    addLoad: `${URLV}load/create`,
    addAccount: `${URLV}account`,
    account: `${URLV}account`,

    cabinet: `${URLV}cabinet`,

    CarShowQueries: `${URLV}showQueries/1`,
    LaodShowQueries: `${URLV}showQueries/2`,
    home: `${URLV}home`,
    contact: `${URLV}contact`,
    aboutUs: `${URLV}aboutUs`,
    announcements: `${URLV}ads`,
    news: `${URLV}news`,
    documents: `${URLV}documents`,
    truckShow: `${URLV}truck/show`,
    loadShow: `${URLV}load/show`,
    message: `${URLV}message`,
    me: `${URLV}me`,
    search: `${URLV}globalSearch`,
    contact: `${URLV}contact`,
    filter: `${URLV}filter`,
    filterCarAll: `${URLV}Truck/getFromCountry`,
    filterLoadAll: `${URLV}Load/getFromCountry`,
    forgotPassword: `${URLV}forget-password`,
    resetPassword: `${URLV}reset-password`,

}
