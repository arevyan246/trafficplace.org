import userSlice from "../features/user/userSlice";
import { configureStore } from "@reduxjs/toolkit";
import languageSlice from "../features/user/languageSlice";

export const store = configureStore({
  reducer: {
    user: userSlice,
    language: languageSlice,
  },
});
