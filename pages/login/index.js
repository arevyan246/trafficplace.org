import Breadcrumbs from "../../components/Breadcrumbs";
import Router from "next/router";
import { useEffect } from "react";
import axios from "axios";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { getToken } from "../../features/user/userSlice";
import Link from "next/link";
import useTranslation from "next-translate/useTranslation";
import Head from "next/head";

const Login = () => {
  let { t } = useTranslation();
  let accountId;
  if (typeof window !== "undefined") {
    accountId = localStorage.getItem("accountId");
  }

  useEffect(() => {
    if (accountId) {
      Router.push("/");
    }
  });

  const dispatch = useDispatch();
  let loading = useSelector((state) => state.user.loading);
  let errorMessage = useSelector((state) => state.user.errorMessage);

  let submitHandler = (values) => {
    dispatch(getToken(values));
    Router.push("/");
  };
  return (
    <>
      <Head>
        <title>{t("common:login")}</title>
      </Head>
      <Breadcrumbs title={t("common:login")} />
      <div className="container-fluid p-5 m-0 form_bg">
        <Formik
          initialValues={{ email: "", password: "" }}
          validate={(values) => {
            const errors = {};
            if (!values.email) {
              errors.email = t("common:emailRequired");
            } else if (
              !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
            ) {
              errors.email = t("common:emailIncomplete");
            }
            if (!values.password) {
              errors.password = t("common:emailPassword");
            } else if (values.password.length < 8) {
              errors.password = t("common:emailPasswordV8");
            }
            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            console.log(values, "values");
            submitHandler(values);
            setSubmitting(false);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            dirty,
          }) => {
            return (
              <form
                onSubmit={handleSubmit}
                className="col-12 col-md-6 p-3 mx-auto border shadow rounded bg-light"
              >
                <div className="form-group">
                  <small>
                    <label htmlFor="LoginEmail1">{t("common:yourEmail")}</label>
                  </small>
                  <input
                    onChange={handleChange}
                    defaultValue={values.email}
                    name="email"
                    type="email"
                    className="form-control"
                    id="LoginEmail1"
                    aria-describedby="emailHelp"
                    placeholder="Email"
                  />
                  <p className="text-danger">
                    {errors.email && touched.email && errors.email}
                  </p>
                </div>

                <div className="form-group mt-2">
                  <small>
                    <label htmlFor="LoginPassword1">
                      {t("common:yourPassword")}
                    </label>
                  </small>
                  <input
                    onChange={handleChange}
                    defaultValue={values.password}
                    name="password"
                    type="password"
                    className="form-control"
                    id="LoginPassword1"
                    placeholder={t("common:yourPassword")}
                  />
                  <p className="text-danger">
                    {errors.password && touched.password && errors.password}
                  </p>
                </div>
                <div className="text-end mt-4">
                  <button type="submit" className="btn btn-danger">
                    {t("common:login")}
                    {loading && (
                      <i className="fa-solid fa-spinner fa-spin-pulse"></i>
                    )}
                  </button>
                </div>

                <div className="form-group mt-2">
                  <small>
                    <Link href="/forgot-password">
                      <a className="form-text text-muted">
                        {t("common:forgotPassword")}
                      </a>
                    </Link>
                  </small>
                </div>
                <p>{errorMessage && errorMessage}</p>
              </form>
            );
          }}
        </Formik>
      </div>
    </>
  );
};
export default Login;
