import styles from "./Search.module.css";
import Breadcrumbs from "../../components/Breadcrumbs";
import useTranslation from "next-translate/useTranslation";
import { useSelector } from "react-redux";
import apiUrl from "../../configs/api-url";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import axios from "axios";
import MessagesModal from "../../components/Modal/messages";
import Head from "next/head";

const Search = () => {
  let [data, setData] = useState('');
  let router = useRouter();
  let { s } = router.query
  let { t } = useTranslation();
  const token = useSelector((state) => state.user.token);
  const language = useSelector((state) => state.language.language);
  const [modalOpen, setModalOpen] = useState(false);
  let handleCloseModal = () => {
    setModalOpen(false);
  };
  const toggleViewInfo = (index) => {
    $(`#viewInfo${index}`).toggleClass("d-none");
  };

  useEffect(() => {
    axios
      .post(apiUrl.search, {
        text: s,
      },
        {
          headers: {
            Authorization: "bearer " + token,
          },
        })
      .then((res) => (setData(res.data.message)))
      .catch((e) => console.log(e.message))
  }, []);
  console.log(data, 'userInfo.name')
  return (
    <>
    <Head>
      <title>{t("common:search")}</title>
    </Head>
      <Breadcrumbs title={t("common:search")} />
      <div className="container mb-5">
        <nav>
          <div className="nav nav-tabs mt-3" id="nav-tab" role="tablist">
            <button className="nav-link text-dark active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">
              {t("common:cars")}
            </button>
            <button className="nav-link text-dark" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">
              {t("common:loads")}
            </button>
          </div>
        </nav>
        <div className="tab-content" id="nav-tabContent">
          <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
            <div className="row pt-3">
              {data &&
                data.load.map((e) => {
                  return (
                    <div className="col-6 col-md-3row mt-3 border rounded shadow-sm m-auto" key={e.id}>
                      {/* <MessagesModal open={modalOpen} closeModalMessage={handleCloseModal} userInfo={e.account.user} selfUser={e.account.self_user.id} /> */}
                      <div className="row">
                        <div className="p-2 col-12 col-md-4 d-flex align-items-center">
                          <img src={`https://maps.googleapis.com/maps/api/staticmap?zoom=4&size=1000x1000&path=color:black|weight:5|${e.destination.from_lat},${e.destination.from_lon}|${e.destination.to_lat},${e.destination.to_lon}&key=AIzaSyAw2ttThDNYHU_XPe5t3HQgQS97f5dOxck`}
                            className={styles.announcements_img} />
                        </div>
                        <div className="p-2 col-12 col-md-8">
                          <div className="position-relative">
                            <div className={styles.label}>{t('common:date')}</div>
                            <div className={styles.date}>
                              {e.destination.date_from} - {e.destination.date_to}
                            </div>
                            <div className={styles.route}>
                              <div className="d-flex">
                                <div className={styles.route_item}>
                                  <div className={styles.label}>{t('common:pointDeparture')}:</div>
                                  <span className="text-capitalize">
                                    {e.destination.from}
                                  </span>
                                </div>
                                <div className={styles.arr}></div>
                                <div className={styles.route_item}>
                                  <div className={styles.label}>{t('common:pointArrival')}:</div>
                                  <span className="text-capitalize">
                                    {e.destination.to}
                                  </span>
                                </div>
                              </div>
                              <div className={styles.route_link}>
                                {t('common:routeCalculation')}
                                <span> {e.distance} {t('common:km')}</span>{" "}
                              </div>
                            </div>
                            <div className={styles.details}>
                              <div className={styles.detailsBlock}>
                                <div className={styles.label}>Груз</div>
                                <span className="d">{e.weight_capacity} т</span>&nbsp;
                                <span className="d">
                                  {e.volume} м<sup>3</sup>
                                </span>{" "}
                                {e.weight}
                              </div>
                              {/* <div className={styles.detailsBlock}>
                                  <div className={styles.label}>Оплата</div>
                                  б/нал{" "}
                                </div> */}
                              <div className={styles.detailsBlock}>
                                <span
                                  className={styles.view_info_btn}
                                  onClick={() => toggleViewInfo(e.id)}
                                >
                                  {t('common:cargoDetails')}
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="d-none" id={`viewInfo${e.id}`}>
                        <div className={styles.details_info}>
                          <div className={styles.company_info}>
                            <div className={styles.user_info}>
                              <div className="row w-100">
                              <div className="col-12 col-md-4 mb-2">
                                <img
                                  className="ms-0 m-1"
                                  src={e.account?.avatar} width='50' />
                              </div>
                              <div className="col-12 col-md-8 mb-2">
                                {e.account?.name}{" "}
                                {e.account?.lastname}{" "}
                                <hr/>
                                {e.account?.self_user.email && (
                                  <p>Email -
                                    <a href={`mailto:${e.account?.self_user.email}`}> {e.account?.self_user.email}</a>
                                  </p>
                                )}
                                {e.account?.skype && 
                                <p>Skype - 
                                  <a href={`skype:${e.account?.skype}?call`}> {e.account?.skype}</a>
                                </p>}
                                {e.account?.telegram && 
                                <p>Telegram - 
                                  <a href={`https://t.me/${e.account?.telegram}`}> {e.account?.telegram}</a>
                                </p>}
                              </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )
                })}
            </div>
          </div>
          <div className="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
          <div className="row pt-3">
              {data &&
                data.truck.map((e) => {
                  return (
                    <div className="col-6 col-md-3row mt-3 border rounded shadow-sm m-auto" key={e.id}>
                      {/* <MessagesModal open={modalOpen} closeModalMessage={handleCloseModal} userInfo={e.account.user} selfUser={e.account.self_user.id} /> */}
                      <div className="row">
                        <div className="p-2 col-12 col-md-4 d-flex align-items-center">
                          <img src={`https://maps.googleapis.com/maps/api/staticmap?zoom=4&size=1000x1000&path=color:black|weight:5|${e.destination.from_lat},${e.destination.from_lon}|${e.destination.to_lat},${e.destination.to_lon}&key=AIzaSyAw2ttThDNYHU_XPe5t3HQgQS97f5dOxck`}
                            className={styles.announcements_img} />
                        </div>
                        <div className="p-2 col-12 col-md-8">
                          <div className="position-relative">
                            <div className={styles.label}>{t('common:date')}</div>
                            <div className={styles.date}>
                              {e.destination.date_from} - {e.destination.date_to}
                            </div>
                            <div className={styles.route}>
                              <div className="d-flex">
                                <div className={styles.route_item}>
                                  <div className={styles.label}>{t('common:pointDeparture')}:</div>
                                  <span className="text-capitalize">
                                    {e.destination.from}
                                  </span>
                                </div>
                                <div className={styles.arr}></div>
                                <div className={styles.route_item}>
                                  <div className={styles.label}>{t('common:pointArrival')}:</div>
                                  <span className="text-capitalize">
                                    {e.destination.to}
                                  </span>
                                </div>
                              </div>
                              <div className={styles.route_link}>
                                {t('common:routeCalculation')}
                                <span> {e.distance} {t('common:km')}</span>{" "}
                              </div>
                            </div>
                            <div className={styles.details}>
                              <div className={styles.detailsBlock}>
                                <div className={styles.label}>Груз</div>
                                <span className="d">{e.weight_capacity} т</span>&nbsp;
                                <span className="d">
                                  {e.volume} м<sup>3</sup>
                                </span>{" "}
                                {e.weight}
                              </div>
                              {/* <div className={styles.detailsBlock}>
                                  <div className={styles.label}>Оплата</div>
                                  б/нал{" "}
                                </div> */}
                              <div className={styles.detailsBlock}>
                                <span
                                  className={styles.view_info_btn}
                                  onClick={() => toggleViewInfo(e.id)}
                                >
                                  {t('common:cargoDetails')}
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="d-none" id={`viewInfo${e.id}`}>
                        <div className={styles.details_info}>
                          <div className={styles.company_info}>
                            <div className={styles.user_info}>
                              <div>
                                <img
                                  className="ms-0 m-1"
                                  src={e.account?.avatar} width='50' />
                              </div>
                              <div>
                                {e.account?.name}{" "}
                                {e.account?.lastname}{" "}
                                {e.account?.surname}{" "}
                                {e.account?.name}{" "}
                              </div>
                              <div>
                                <button
                                  className="btn btn-outline-primary m-1"
                                  type="button"
                                  onClick={() => setModalOpen(!modalOpen)}
                                >
                                  {t("common:write")}
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  )
                })}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};



export default Search;
