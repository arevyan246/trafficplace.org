import Breadcrumbs from "../../components/Breadcrumbs";
import { useRouter } from "next/router";
import useTranslation from "next-translate/useTranslation";
import { useEffect } from "react";
import axios from "axios";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { getToken } from "../../features/user/userSlice";
import Link from "next/link";
import apiUrl from "../../configs/api-url";
import Head from "next/head";

const ResetPassword = () => {
  const router = useRouter();
  let token = router?.query?.token;
  let { t } = useTranslation();
  let accountId;
  if (typeof window !== "undefined") {
    accountId = localStorage.getItem("accountId");
  }
  const dispatch = useDispatch();
  let loading = useSelector((state) => state.user.loading);
  let errorMessage = useSelector((state) => state.user.errorMessage);

  let submitHandler = (values) => {
    console.log(values);
    axios
      .post(apiUrl.resetPassword, {
        password: values.password,
        password_confirmation: values.password,
        token: token,
      })
      .then((res) => {
        console.log(res, "forgot");
      })
      .catch((err) => {
        console.log(err, "err forgot");
      });
  };
  console.log(router, "router");
  if (!token) {
    return <h1>Token is not Founded</h1>;
  }

  return (
    <>
      <Head>
        <title>{t("common:resetPassword")}</title>
      </Head>
      <Breadcrumbs title={t("common:resetPassword")} />
      <div className="container-fluid p-5 m-0 form_bg">
        <Formik
          initialValues={{ password: "", password_confirmation: "" }}
          validate={(values) => {
            const errors = {};
            if (!values.password) {
              errors.password = "password is required";
            } else if (!values.password_confirmation) {
              errors.password_confirmation = "please confirm password";
            } else if (values.password !== values.password_confirmation) {
              errors.password_confirmation = "passwords not match";
            }
            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            console.log(values, "values");
            submitHandler(values);
            setSubmitting(false);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            dirty,
          }) => {
            return (
              <form
                onSubmit={handleSubmit}
                className="col-12 col-md-6 p-3 mx-auto border shadow rounded bg-light"
              >
                <div className="form-group">
                  <small>
                    <label htmlFor="ForgotPasswordp">
                      {t("common:yourPassword")}
                    </label>
                  </small>
                  <input
                    onChange={handleChange}
                    defaultValue={values.password}
                    name="password"
                    type="password"
                    className="form-control"
                    id="ForgotPasswordp"
                    placeholder={t("common:yourPassword")}
                  />
                  <p className="text-danger">
                    {errors.password && touched.password && errors.password}
                  </p>
                  <small>
                    <label htmlFor="ForgotPasswordp2">
                      {t("common:yourPassword")}
                    </label>
                  </small>
                  <input
                    onChange={handleChange}
                    defaultValue={values.password_confirmation}
                    name="password_confirmation"
                    type="password"
                    className="form-control"
                    id="ForgotPasswordp2"
                    placeholder={t("common:yourPassword")}
                  />
                  <p className="text-danger">
                    {errors.password_confirmation &&
                      touched.password_confirmation &&
                      errors.password_confirmation}
                  </p>
                </div>
                <div className="text-end mt-4">
                  <button type="submit" className="btn btn-danger">
                    {t("common:restore")}
                    {loading && (
                      <i className="fa-solid fa-spinner fa-spin-pulse"></i>
                    )}
                  </button>
                </div>
                {/* <p>{errorMessage && errorMessage}</p> */}
              </form>
            );
          }}
        </Formik>
      </div>
    </>
  );
};
export default ResetPassword;
