import styles from "./Loads.module.css";
import Breadcrumbs from "../../components/Breadcrumbs";
import SearchForm from "../../components/SearchForm";
import AboutTheSystem from "../../components/AboutTheSystem";
import CtfacetopMenu from "../../components/Ctfacetopmenu";
import ScrollButton from "../../components/ScrollButton";
import Link from "next/link";
import { useSelector } from "react-redux";
import SuccessModal from "../../components/Modal";
import useTranslation from "next-translate/useTranslation";
import { useContext, useEffect, useState } from "react";
import axios from "axios";
import Image from "next/image";
import MessagesModal from "../../components/Modal/messages";
import apiUrl from "../../configs/api-url";
import { useRouter } from "next/router";
import Head from "next/head";

const Loads = () => {
  let router = useRouter();
  const [data, setData] = useState("");
  let { t } = useTranslation();
  const [modalOpen, setModalOpen] = useState(false);
  const [countryList, setCountryList] = useState([]);
  let userToken;
  let accountId;
  let message;
  const toggleViewInfo = (index) => {
    $(`#viewInfo${index}`).toggleClass("d-none");
  };
  let handleCloseModal = () => {
    setModalOpen(false);
  };
  if (typeof window !== "undefined") {
    message = localStorage.getItem("message");
    userToken = localStorage.getItem("token");
    accountId = localStorage.getItem("accountId");
    setTimeout(() => {
      localStorage.removeItem("message");
    }, 1000);
  }
  let filterResultHandler = (value) => {
    setData(value);
  };
  let getFromCountry = () => {
    axios
      .get(apiUrl.filterLoadAll)
      .then((res) => {
        setCountryList(res.data.message),
          console.log(res.data.message, "res.data.message");
      })
      .catch((e) => console.log(e.message));
  };
  let getByDestination = (from) => {
    axios
      .post(apiUrl.filter, {
        type: "Load",
        from: from,
        destination: "from",
      })
      .then((res) => {
        setData(res.data.message);
      })
      .catch((e) => console.log(e.message));
  };
  let getAllCars = () => {
    axios
      .post(apiUrl.filter, {
        type: "Load",
      })
      .then((res) => {
        setData(res.data.message);
      })
      .catch((e) => console.log(e.message));
  };
  let getForArmenia = () => {
    axios
      .post(apiUrl.filter, {
        type: "Load",
        from: "armenia",
        to: "armenita",
      })
      .then((res) => {
        setData(res.data.message);
      })
      .catch((e) => console.log(e.message));
  };
  useEffect(() => {
    getFromCountry();
    axios
      .get(apiUrl.loadShow, {
        headers: {
          Authorization: "bearer " + userToken,
        },
      })
      .then((res) => setData(res.data.message))
      .catch((e) => console.log(e.message));
  }, []);
  console.log(data, "Ladedata");
  return (
    <>
      <Head>
        <title>{t("common:loads")}</title>
      </Head>
      <Breadcrumbs title={t("common:loads")} text={t("common:loadsTitle")} />
      {message && <SuccessModal massage={t("common:successLoad")} />}
      <div className="container mt-3">
        <div className="row">
          <div className="col-md-3 d-none d-md-block">
            {data && (
              <>
                <ul>
                  <li className="mb-2">
                    <button
                      type="button"
                      onClick={getAllCars}
                      className="btn btn-outline-primary"
                    >
                      {t("common:allCar")}
                    </button>
                  </li>
                  <li className="mb-2">
                    <button
                      type="button"
                      onClick={getForArmenia}
                      className="btn btn-outline-primary"
                    >
                      {t("common:forArmenia")}
                    </button>
                  </li>
                </ul>
                <h3 className="mb-3 text-capitalize">{t("common:where")}</h3>
              </>
            )}
            <ul>
              {countryList ? (
                countryList.map((item, index) => {
                  return (
                    <li
                      className="mb-2"
                      key={index}
                      onClick={() => getByDestination(item.from)}
                    >
                      <button
                        type="button"
                        className="btn btn-outline-success text-capitalize"
                      >
                        {item?.from} - {item?.count}
                      </button>
                    </li>
                  );
                })
              ) : (
                <h4>{t("common:nothingFound")}</h4>
              )}
            </ul>
          </div>
          <div className="col-12 col-md-9">
            <div className="container">
              <div className="row">
                <SearchForm filterResultHandler={filterResultHandler} />

                <div className="col-12 mt-3">
                  <h3>{t("common:loadsContainerTitle")}</h3>
                  <p>{t("common:loadsContainerText")}</p>
                  <div className="row">
                    <div className="col-12 col-md-4">
                      <Link href="/">
                        <a className={`${styles.cyan_bg} ${styles.all_cargo}`}>
                          {t("common:allCargo")}
                          <span className={styles.all_cargo_count}>4478</span>
                          <svg
                            className={styles.cyan}
                            width="30"
                            height="30"
                            viewBox="0 0 30 30"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <circle cx="15" cy="15" r="15"></circle>
                            <path
                              d="M6.88408 8.29692C7.0036 8.09967 7.23561 7.98913 7.46411 8.00085C10.9388 8.00124 14.4135 8.00046 17.8886 8.00124C18.1363 7.9903 18.3812 8.13326 18.487 8.35863C18.605 8.64337 18.5296 8.96092 18.5515 9.26012C19.4381 9.26168 20.3244 9.25856 21.211 9.26168C21.4657 9.25309 21.7075 9.40698 21.8157 9.63626C22.3844 10.7619 22.9492 11.89 23.5167 13.0165C23.596 13.1692 23.6624 13.3309 23.7682 13.4684C24.1225 13.9226 24.4756 14.3777 24.8287 14.8327C24.9279 14.9577 24.9892 15.11 24.9994 15.2702V19.3925C24.9873 19.7155 24.7014 19.9963 24.3768 19.9936C23.7647 19.9995 23.1527 19.9936 22.5406 19.9963C22.448 20.3194 22.3004 20.6275 22.091 20.8912C21.659 21.4513 20.9818 21.8138 20.2752 21.8571C19.674 21.8997 19.0596 21.7169 18.5819 21.3494C18.1386 21.0154 17.8164 20.5307 17.6691 19.9963C16.422 19.9956 15.1752 19.9956 13.9284 19.9963C13.823 20.3682 13.6394 20.7186 13.382 21.008C12.9453 21.5079 12.3047 21.8228 11.6419 21.8583C11.0427 21.8958 10.4322 21.7099 9.95843 21.3408C9.52019 21.0076 9.20303 20.5256 9.05577 19.9963C8.50308 19.9932 7.9504 19.9999 7.39771 19.9932C7.07703 19.9901 6.79073 19.7077 6.78916 19.3859C6.76807 19.0499 7.05984 18.7402 7.39536 18.7375C7.94571 18.73 8.49644 18.7386 9.04718 18.7336C9.27216 17.802 10.0709 17.0419 11.0146 16.8708C11.6173 16.754 12.2622 16.8646 12.7875 17.1845C13.3562 17.5219 13.7769 18.0934 13.9378 18.7343C15.1783 18.7351 16.4185 18.7351 17.659 18.7343C17.7812 18.2563 18.0425 17.8145 18.4089 17.4825C18.8128 17.1134 19.3393 16.8798 19.8854 16.8353C20.4912 16.7798 21.1142 16.9548 21.6012 17.3188C22.0692 17.6622 22.4082 18.1719 22.5504 18.734C22.946 18.7359 23.3421 18.7347 23.7382 18.7343C23.7346 17.6524 23.746 16.5697 23.7331 15.4885C23.4405 15.0854 23.123 14.6987 22.8199 14.3031C21.1899 14.3023 19.56 14.3046 17.9304 14.3019C17.5957 14.312 17.2883 14.0203 17.2922 13.6836C17.2867 12.2095 17.2922 10.735 17.2895 9.2609C14.0128 9.26051 10.7365 9.2609 7.4602 9.2609C7.286 9.26559 7.10828 9.20934 6.9829 9.08513C6.76846 8.88984 6.72628 8.53986 6.88408 8.29692ZM18.5511 10.5217C18.5511 11.3619 18.5507 12.2017 18.5515 13.0418C19.7408 13.0422 20.9302 13.0414 22.1195 13.0422C21.6942 12.2028 21.2743 11.3611 20.8501 10.5213C20.0838 10.5217 19.3174 10.521 18.5511 10.5217ZM11.2255 18.116C10.8701 18.1918 10.5529 18.4305 10.3842 18.7527C10.2151 19.0628 10.1889 19.4468 10.3131 19.7776C10.4494 20.1577 10.7814 20.458 11.1716 20.5592C11.7141 20.7131 12.3348 20.4432 12.5953 19.9444C12.8742 19.4558 12.7609 18.791 12.3414 18.4184C12.0473 18.1445 11.6177 18.0297 11.2255 18.116ZM19.8229 18.1195C19.196 18.2492 18.7472 18.9125 18.864 19.5429C18.9491 20.1538 19.5385 20.6381 20.1553 20.5998C20.5841 20.5861 20.9942 20.3334 21.2009 19.9577C21.3993 19.6108 21.4149 19.1675 21.2407 18.8082C21.0048 18.284 20.381 17.9813 19.8229 18.1195Z"
                              fill="white"
                            ></path>
                            <path
                              d="M6.45896 10.62C6.58043 10.5797 6.71011 10.5868 6.83627 10.586C8.56699 10.5875 10.2977 10.5844 12.028 10.5871C12.3557 10.5762 12.6561 10.857 12.6643 11.1851C12.6909 11.5332 12.3811 11.8562 12.0323 11.8449C10.2465 11.8472 8.46114 11.8464 6.67574 11.8452C6.37967 11.8566 6.09688 11.6335 6.04337 11.3414C5.97463 11.0406 6.16484 10.7121 6.45896 10.62Z"
                              fill="white"
                            ></path>
                            <path
                              d="M5 13.6659C5.01172 13.3409 5.29958 13.0589 5.62651 13.0636C7.39746 13.0593 9.1688 13.0632 10.9397 13.0612C11.1444 13.0526 11.3581 13.1245 11.4885 13.2882C11.6803 13.5069 11.685 13.8623 11.4979 14.0854C11.3678 14.2564 11.1479 14.3322 10.9382 14.3232C9.16724 14.3209 7.3959 14.326 5.62495 14.3209C5.29451 14.3252 5.00391 14.0362 5 13.7069V13.6659Z"
                              fill="white"
                            ></path>
                            <path
                              d="M6.45737 15.5793C6.56556 15.5418 6.68157 15.543 6.79484 15.543C7.71937 15.5457 8.6443 15.5402 9.56883 15.5457C9.90396 15.5434 10.1989 15.8465 10.1856 16.1816C10.1895 16.5109 9.8981 16.8046 9.56844 16.8026C8.60446 16.8065 7.64008 16.8046 6.67571 16.8034C6.39526 16.8116 6.12341 16.6152 6.05389 16.3417C5.95897 16.0328 6.14841 15.6738 6.45737 15.5793Z"
                              fill="white"
                            ></path>
                          </svg>
                        </a>
                      </Link>
                    </div>
                    <div className="col-12 col-md-4">
                      <Link href="/">
                        <a
                          className={`${styles.yellow_bg} ${styles.all_cargo}`}
                        >
                          {t("common:international")}
                          <span className={styles.all_cargo_count}>469</span>
                          <svg
                            className={styles.yellow}
                            width="30"
                            height="30"
                            viewBox="0 0 30 30"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <circle cx="15" cy="15" r="15"></circle>
                            <path
                              d="M6.88408 8.29692C7.0036 8.09967 7.23561 7.98913 7.46411 8.00085C10.9388 8.00124 14.4135 8.00046 17.8886 8.00124C18.1363 7.9903 18.3812 8.13326 18.487 8.35863C18.605 8.64337 18.5296 8.96092 18.5515 9.26012C19.4381 9.26168 20.3244 9.25856 21.211 9.26168C21.4657 9.25309 21.7075 9.40698 21.8157 9.63626C22.3844 10.7619 22.9492 11.89 23.5167 13.0165C23.596 13.1692 23.6624 13.3309 23.7682 13.4684C24.1225 13.9226 24.4756 14.3777 24.8287 14.8327C24.9279 14.9577 24.9892 15.11 24.9994 15.2702V19.3925C24.9873 19.7155 24.7014 19.9963 24.3768 19.9936C23.7647 19.9995 23.1527 19.9936 22.5406 19.9963C22.448 20.3194 22.3004 20.6275 22.091 20.8912C21.659 21.4513 20.9818 21.8138 20.2752 21.8571C19.674 21.8997 19.0596 21.7169 18.5819 21.3494C18.1386 21.0154 17.8164 20.5307 17.6691 19.9963C16.422 19.9956 15.1752 19.9956 13.9284 19.9963C13.823 20.3682 13.6394 20.7186 13.382 21.008C12.9453 21.5079 12.3047 21.8228 11.6419 21.8583C11.0427 21.8958 10.4322 21.7099 9.95843 21.3408C9.52019 21.0076 9.20303 20.5256 9.05577 19.9963C8.50308 19.9932 7.9504 19.9999 7.39771 19.9932C7.07703 19.9901 6.79073 19.7077 6.78916 19.3859C6.76807 19.0499 7.05984 18.7402 7.39536 18.7375C7.94571 18.73 8.49644 18.7386 9.04718 18.7336C9.27216 17.802 10.0709 17.0419 11.0146 16.8708C11.6173 16.754 12.2622 16.8646 12.7875 17.1845C13.3562 17.5219 13.7769 18.0934 13.9378 18.7343C15.1783 18.7351 16.4185 18.7351 17.659 18.7343C17.7812 18.2563 18.0425 17.8145 18.4089 17.4825C18.8128 17.1134 19.3393 16.8798 19.8854 16.8353C20.4912 16.7798 21.1142 16.9548 21.6012 17.3188C22.0692 17.6622 22.4082 18.1719 22.5504 18.734C22.946 18.7359 23.3421 18.7347 23.7382 18.7343C23.7346 17.6524 23.746 16.5697 23.7331 15.4885C23.4405 15.0854 23.123 14.6987 22.8199 14.3031C21.1899 14.3023 19.56 14.3046 17.9304 14.3019C17.5957 14.312 17.2883 14.0203 17.2922 13.6836C17.2867 12.2095 17.2922 10.735 17.2895 9.2609C14.0128 9.26051 10.7365 9.2609 7.4602 9.2609C7.286 9.26559 7.10828 9.20934 6.9829 9.08513C6.76846 8.88984 6.72628 8.53986 6.88408 8.29692ZM18.5511 10.5217C18.5511 11.3619 18.5507 12.2017 18.5515 13.0418C19.7408 13.0422 20.9302 13.0414 22.1195 13.0422C21.6942 12.2028 21.2743 11.3611 20.8501 10.5213C20.0838 10.5217 19.3174 10.521 18.5511 10.5217ZM11.2255 18.116C10.8701 18.1918 10.5529 18.4305 10.3842 18.7527C10.2151 19.0628 10.1889 19.4468 10.3131 19.7776C10.4494 20.1577 10.7814 20.458 11.1716 20.5592C11.7141 20.7131 12.3348 20.4432 12.5953 19.9444C12.8742 19.4558 12.7609 18.791 12.3414 18.4184C12.0473 18.1445 11.6177 18.0297 11.2255 18.116ZM19.8229 18.1195C19.196 18.2492 18.7472 18.9125 18.864 19.5429C18.9491 20.1538 19.5385 20.6381 20.1553 20.5998C20.5841 20.5861 20.9942 20.3334 21.2009 19.9577C21.3993 19.6108 21.4149 19.1675 21.2407 18.8082C21.0048 18.284 20.381 17.9813 19.8229 18.1195Z"
                              fill="white"
                            ></path>
                            <path
                              d="M6.45896 10.62C6.58043 10.5797 6.71011 10.5868 6.83627 10.586C8.56699 10.5875 10.2977 10.5844 12.028 10.5871C12.3557 10.5762 12.6561 10.857 12.6643 11.1851C12.6909 11.5332 12.3811 11.8562 12.0323 11.8449C10.2465 11.8472 8.46114 11.8464 6.67574 11.8452C6.37967 11.8566 6.09688 11.6335 6.04337 11.3414C5.97463 11.0406 6.16484 10.7121 6.45896 10.62Z"
                              fill="white"
                            ></path>
                            <path
                              d="M5 13.6659C5.01172 13.3409 5.29958 13.0589 5.62651 13.0636C7.39746 13.0593 9.1688 13.0632 10.9397 13.0612C11.1444 13.0526 11.3581 13.1245 11.4885 13.2882C11.6803 13.5069 11.685 13.8623 11.4979 14.0854C11.3678 14.2564 11.1479 14.3322 10.9382 14.3232C9.16724 14.3209 7.3959 14.326 5.62495 14.3209C5.29451 14.3252 5.00391 14.0362 5 13.7069V13.6659Z"
                              fill="white"
                            ></path>
                            <path
                              d="M6.45737 15.5793C6.56556 15.5418 6.68157 15.543 6.79484 15.543C7.71937 15.5457 8.6443 15.5402 9.56883 15.5457C9.90396 15.5434 10.1989 15.8465 10.1856 16.1816C10.1895 16.5109 9.8981 16.8046 9.56844 16.8026C8.60446 16.8065 7.64008 16.8046 6.67571 16.8034C6.39526 16.8116 6.12341 16.6152 6.05389 16.3417C5.95897 16.0328 6.14841 15.6738 6.45737 15.5793Z"
                              fill="white"
                            ></path>
                          </svg>
                        </a>
                      </Link>
                    </div>
                    <div className="col-12 col-md-4">
                      <Link href="/">
                        <a className={`${styles.green_bg} ${styles.all_cargo}`}>
                          {t("common:forArmenia")}
                          <span className={styles.all_cargo_count}>454</span>
                          <svg
                            className={styles.green}
                            width="30"
                            height="30"
                            viewBox="0 0 30 30"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <circle cx="15" cy="15" r="15"></circle>
                            <path
                              d="M6.88408 8.29692C7.0036 8.09967 7.23561 7.98913 7.46411 8.00085C10.9388 8.00124 14.4135 8.00046 17.8886 8.00124C18.1363 7.9903 18.3812 8.13326 18.487 8.35863C18.605 8.64337 18.5296 8.96092 18.5515 9.26012C19.4381 9.26168 20.3244 9.25856 21.211 9.26168C21.4657 9.25309 21.7075 9.40698 21.8157 9.63626C22.3844 10.7619 22.9492 11.89 23.5167 13.0165C23.596 13.1692 23.6624 13.3309 23.7682 13.4684C24.1225 13.9226 24.4756 14.3777 24.8287 14.8327C24.9279 14.9577 24.9892 15.11 24.9994 15.2702V19.3925C24.9873 19.7155 24.7014 19.9963 24.3768 19.9936C23.7647 19.9995 23.1527 19.9936 22.5406 19.9963C22.448 20.3194 22.3004 20.6275 22.091 20.8912C21.659 21.4513 20.9818 21.8138 20.2752 21.8571C19.674 21.8997 19.0596 21.7169 18.5819 21.3494C18.1386 21.0154 17.8164 20.5307 17.6691 19.9963C16.422 19.9956 15.1752 19.9956 13.9284 19.9963C13.823 20.3682 13.6394 20.7186 13.382 21.008C12.9453 21.5079 12.3047 21.8228 11.6419 21.8583C11.0427 21.8958 10.4322 21.7099 9.95843 21.3408C9.52019 21.0076 9.20303 20.5256 9.05577 19.9963C8.50308 19.9932 7.9504 19.9999 7.39771 19.9932C7.07703 19.9901 6.79073 19.7077 6.78916 19.3859C6.76807 19.0499 7.05984 18.7402 7.39536 18.7375C7.94571 18.73 8.49644 18.7386 9.04718 18.7336C9.27216 17.802 10.0709 17.0419 11.0146 16.8708C11.6173 16.754 12.2622 16.8646 12.7875 17.1845C13.3562 17.5219 13.7769 18.0934 13.9378 18.7343C15.1783 18.7351 16.4185 18.7351 17.659 18.7343C17.7812 18.2563 18.0425 17.8145 18.4089 17.4825C18.8128 17.1134 19.3393 16.8798 19.8854 16.8353C20.4912 16.7798 21.1142 16.9548 21.6012 17.3188C22.0692 17.6622 22.4082 18.1719 22.5504 18.734C22.946 18.7359 23.3421 18.7347 23.7382 18.7343C23.7346 17.6524 23.746 16.5697 23.7331 15.4885C23.4405 15.0854 23.123 14.6987 22.8199 14.3031C21.1899 14.3023 19.56 14.3046 17.9304 14.3019C17.5957 14.312 17.2883 14.0203 17.2922 13.6836C17.2867 12.2095 17.2922 10.735 17.2895 9.2609C14.0128 9.26051 10.7365 9.2609 7.4602 9.2609C7.286 9.26559 7.10828 9.20934 6.9829 9.08513C6.76846 8.88984 6.72628 8.53986 6.88408 8.29692ZM18.5511 10.5217C18.5511 11.3619 18.5507 12.2017 18.5515 13.0418C19.7408 13.0422 20.9302 13.0414 22.1195 13.0422C21.6942 12.2028 21.2743 11.3611 20.8501 10.5213C20.0838 10.5217 19.3174 10.521 18.5511 10.5217ZM11.2255 18.116C10.8701 18.1918 10.5529 18.4305 10.3842 18.7527C10.2151 19.0628 10.1889 19.4468 10.3131 19.7776C10.4494 20.1577 10.7814 20.458 11.1716 20.5592C11.7141 20.7131 12.3348 20.4432 12.5953 19.9444C12.8742 19.4558 12.7609 18.791 12.3414 18.4184C12.0473 18.1445 11.6177 18.0297 11.2255 18.116ZM19.8229 18.1195C19.196 18.2492 18.7472 18.9125 18.864 19.5429C18.9491 20.1538 19.5385 20.6381 20.1553 20.5998C20.5841 20.5861 20.9942 20.3334 21.2009 19.9577C21.3993 19.6108 21.4149 19.1675 21.2407 18.8082C21.0048 18.284 20.381 17.9813 19.8229 18.1195Z"
                              fill="white"
                            ></path>
                            <path
                              d="M6.45896 10.62C6.58043 10.5797 6.71011 10.5868 6.83627 10.586C8.56699 10.5875 10.2977 10.5844 12.028 10.5871C12.3557 10.5762 12.6561 10.857 12.6643 11.1851C12.6909 11.5332 12.3811 11.8562 12.0323 11.8449C10.2465 11.8472 8.46114 11.8464 6.67574 11.8452C6.37967 11.8566 6.09688 11.6335 6.04337 11.3414C5.97463 11.0406 6.16484 10.7121 6.45896 10.62Z"
                              fill="white"
                            ></path>
                            <path
                              d="M5 13.6659C5.01172 13.3409 5.29958 13.0589 5.62651 13.0636C7.39746 13.0593 9.1688 13.0632 10.9397 13.0612C11.1444 13.0526 11.3581 13.1245 11.4885 13.2882C11.6803 13.5069 11.685 13.8623 11.4979 14.0854C11.3678 14.2564 11.1479 14.3322 10.9382 14.3232C9.16724 14.3209 7.3959 14.326 5.62495 14.3209C5.29451 14.3252 5.00391 14.0362 5 13.7069V13.6659Z"
                              fill="white"
                            ></path>
                            <path
                              d="M6.45737 15.5793C6.56556 15.5418 6.68157 15.543 6.79484 15.543C7.71937 15.5457 8.6443 15.5402 9.56883 15.5457C9.90396 15.5434 10.1989 15.8465 10.1856 16.1816C10.1895 16.5109 9.8981 16.8046 9.56844 16.8026C8.60446 16.8065 7.64008 16.8046 6.67571 16.8034C6.39526 16.8116 6.12341 16.6152 6.05389 16.3417C5.95897 16.0328 6.14841 15.6738 6.45737 15.5793Z"
                              fill="white"
                            ></path>
                          </svg>
                        </a>
                      </Link>
                    </div>
                  </div>
                </div>
                <div className="col-12 mt-3">
                  <h3>{t("common:cargoInArmenia")}</h3>
                  <div className="row">
                    <CtfacetopMenu name={t("common:cargoOwners")} count="452" />
                    <CtfacetopMenu name={t("common:expeditions")} count="231" />
                    <CtfacetopMenu name={t("common:carriers")} count="845" />
                  </div>
                </div>
                <div className="col-12 mt-3">
                  <h3>{t("common:newMachines")}</h3>
                  {data && data.length !== 0 ? (
                    data.map((e) => {
                      console.log(e, "e");
                      return (
                        <div
                          className="row mt-3 border rounded shadow-sm m-auto"
                          key={e.id}
                        >
                          {/* <MessagesModal
                            open={modalOpen}
                            closeModalMessage={handleCloseModal}
                            userInfo={e.account.user}
                            selfUser={e.account.self_user.id}
                          /> */}
                          <div className="row">
                            <div className="p-2 col-12 col-md-4 d-flex align-items-center">
                              <img
                                src={`https://maps.googleapis.com/maps/api/staticmap?zoom=4&size=1000x1000&path=color:black|weight:5|${e.destination?.from_lat},${e.destination?.from_lon}|${e.destination?.to_lat},${e.destination?.to_lon}&key=AIzaSyAw2ttThDNYHU_XPe5t3HQgQS97f5dOxck`}
                                className={styles.announcements_img}
                              />
                            </div>
                            <div className="p-2 col-12 col-md-8">
                              <div className="position-relative">
                                <div className={styles.label}>
                                  {t("common:date")}
                                </div>
                                <div className={styles.date}>
                                  {e.destination?.date_from} -{" "}
                                  {e.destination?.date_to}
                                </div>
                                <div className={styles.route}>
                                  <div className="d-flex">
                                    <div className={styles.route_item}>
                                      <div className={styles.label}>
                                        {t("common:pointDeparture")}:
                                      </div>
                                      <span className="text-capitalize">
                                        {e.destination?.from}
                                      </span>
                                    </div>
                                    <div className={styles.arr}></div>
                                    <div className={styles.route_item}>
                                      <div className={styles.label}>
                                        {t("common:pointArrival")}:
                                      </div>
                                      <span className="text-capitalize">
                                        {e.destination?.to}
                                      </span>
                                    </div>
                                  </div>
                                  <div className={styles.route_link}>
                                    {e.destination?.from &&
                                      e.destination?.to && (
                                        <>
                                          {t("common:routeCalculation")}
                                          <span
                                            className="text-success"
                                            style={{ cursor: "pointer" }}
                                            onClick={() => {
                                              router.push({
                                                pathname: "/map",
                                                query: {
                                                  from: e.destination?.from,
                                                  to: e.destination?.to,
                                                },
                                              });
                                            }}
                                          >
                                            {" "}
                                            {e.distance} {t("common:km")}
                                          </span>
                                        </>
                                      )}
                                  </div>
                                </div>
                                <div className={styles.details}>
                                  <div className={styles.detailsBlock}>
                                    <div className={styles.label}>Груз</div>
                                    <span className="d">
                                      {e.weight_capacity} т
                                    </span>
                                    &nbsp;
                                    <span className="d">
                                      {e.volume} м<sup>3</sup>
                                    </span>{" "}
                                    {e.truck_body?.name}
                                  </div>
                                  {/* <div className={styles.detailsBlock}>
                                  <div className={styles.label}>Оплата</div>
                                  б/нал{" "}
                                </div> */}
                                  <div className={styles.detailsBlock}>
                                    <span
                                      className={styles.view_info_btn}
                                      onClick={() => toggleViewInfo(e.id)}
                                    >
                                      {t("common:cargoDetails")}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="d-none" id={`viewInfo${e.id}`}>
                            <div className={styles.details_info}>
                              <div className={styles.company_info}>
                                <div className={styles.user_info}>
                                  <div className="row w-100">
                                    <div className="col-12 col-md-4 mb-2">
                                      <img
                                        className="ms-0 m-1"
                                        src={e.account?.avatar}
                                        width="50"
                                      />
                                    </div>
                                    <div className="col-12 col-md-8 mb-2">
                                      {e.account?.name} {e.account?.lastname}{" "}
                                      <hr />
                                      {e.account?.self_user.email && (
                                        <p>
                                          Email -
                                          <a
                                            href={`mailto:${e.account?.self_user.email}`}
                                          >
                                            {" "}
                                            {e.account?.self_user.email}
                                          </a>
                                        </p>
                                      )}
                                      {e.account?.skype && (
                                        <p>
                                          Skype -
                                          <a
                                            href={`skype:${e.account?.skype}?call`}
                                          >
                                            {" "}
                                            {e.account?.skype}
                                          </a>
                                        </p>
                                      )}
                                      {e.account?.telegram && (
                                        <p>
                                          Telegram -
                                          <a
                                            href={`https://t.me/${e.account?.telegram}`}
                                          >
                                            {" "}
                                            {e.account?.telegram}
                                          </a>
                                        </p>
                                      )}
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    })
                  ) : (
                    <h1>{t("common:nothingFound")}</h1>
                  )}
                </div>
                <AboutTheSystem />
              </div>
            </div>
          </div>
        </div>
      </div>
      <ScrollButton
        urlLink={
          accountId !== "null" ? "/cabinet/add-load" : "cabinet/registration"
        }
        text={t("common:addLoad")}
      />
    </>
  );
};
export default Loads;
