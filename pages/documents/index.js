import styles from "./Documents.module.css";
import Image from "next/image";
import Link from "next/link";
import Breadcrumbs from "../../components/Breadcrumbs";
import useTranslation from "next-translate/useTranslation";
import { useSelector } from "react-redux";
import apiUrl from "../../configs/api-url";
import Head from "next/head";

const Documents = ({ data }) => {
  let { t } = useTranslation();
  const language = useSelector((state) => state.language.language);
  return (
    <>
      <Head>
        <title>{t("common:documents")}</title>
      </Head>
      <Breadcrumbs title={t("common:documents")} />
      <div className="container">
        <div className="row pt-3">
          {data &&
            data.message.map((item) => {
              return (
                <div
                  key={item.id}
                  className={`${styles.document_img} col-4 col-md-2`}
                >
                  <Link
                    href={"https://admin.trafficplace.org/" + item.pdf_file}
                  >
                    <a target="_balnk" title={item["seo_title_" + language]}>
                      <Image src="/document.png" width={512} height={512} />
                      <p className={styles.document_name}>
                        {item["title_" + language]}
                      </p>
                    </a>
                  </Link>
                </div>
              );
            })}

          {/* <div className="col-12 text-center mt-3 mb-3">
            <button className="btn btn-danger">{t("common:showMore")}</button>
          </div> */}
        </div>
      </div>
    </>
  );
};

export async function getStaticProps() {
  const res = await fetch(apiUrl.documents);
  const json = await res.json();
  return {
    props: {
      data: json,
    },
  };
}

export default Documents;
