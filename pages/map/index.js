import { useRouter } from "next/router";
import { useEffect, useRef } from "react";

export default function Map({ data }) {
  let router = useRouter();
  let { from, to } = router.query;
  let mapRef = useRef();

  useEffect(() => {
    function readyTo() {
      console.log(google, "ggoogle");
      if (google) {
        var infowindow = new google.maps.InfoWindow();
        var directions = new google.maps.DirectionsService();
        var renderer = new google.maps.DirectionsRenderer({
          suppressPolylines: true,
          infoWindow: infowindow,
        });
        var map;
        function initialize() {
          var mapOptions = {
            zoom: 13,
            center: new google.maps.LatLng(40.7482333, -73.8681295),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
          };

          map = new google.maps.Map(document.querySelector("#map"), mapOptions);
          route();
        }
        initialize();
        function route() {
          var request = {
            origin: from,
            destination: to,
            travelMode: google.maps.DirectionsTravelMode.DRIVING,
          };

          var panel = document.getElementById("panel");
          panel.innerHTML = "";
          directions.route(request, function (response, status) {
            console.log(response, "himar");
            if (response.status === "ZERO_RESULTS") {
              alert("Route not Founded");
              router.push("/");
            }
            if (status == google.maps.DirectionsStatus.OK) {
              renderer.setDirections(response);
              renderer.setMap(map);
              renderer.setPanel(panel);
              renderDirectionsPolylines(response);
              console.log(renderer.getDirections());
            } else {
              renderer.setMap(null);
              renderer.setPanel(null);
            }
          });
        }

        var polylineOptions = {
          strokeColor: "#C83939",
          strokeOpacity: 1,
          strokeWeight: 4,
        };
        var polylines = [];

        function renderDirectionsPolylines(response) {
          for (let i = 0; i < polylines.length; i++) {
            polylines[i].setMap(null);
          }
          var legs = response.routes[0].legs;
          for (let i = 0; i < legs.length; i++) {
            var steps = legs[i].steps;
            for (let j = 0; j < steps.length; j++) {
              let color;
              // find meters per second and convert to miles per hour
              var mph =
                (steps[j].distance.value / steps[j].duration.value) * 2.23694;
              if (mph >= 55) {
                // route.leg[i].road = highway
                color = "#FF0000";
              } else {
                // route.leg[i].road = city
                color = "black";
              }
              console.log(
                "step " +
                  j +
                  " color=" +
                  color +
                  " mph=" +
                  mph +
                  " dist=" +
                  steps[j].distance.value +
                  " meters/time=" +
                  steps[j].duration.value +
                  " seconds"
              );

              var nextSegment = steps[j].path;
              polylineOptions.strokeColor = color;
              var stepPolyline = new google.maps.Polyline(polylineOptions);
              for (let k = 0; k < nextSegment.length; k++) {
                stepPolyline.getPath().push(nextSegment[k]);
              }
              polylines.push(stepPolyline);
              stepPolyline.setMap(map);
              // route click listeners, different one on each step
              google.maps.event.addListener(
                stepPolyline,
                "click",
                (function (mph) {
                  return function (evt) {
                    infowindow.setContent(
                      "you clicked on the route<br>speed ~= " +
                        mph.toFixed(2) +
                        " mph<br>" +
                        evt.latLng.toUrlValue(6)
                    );
                    infowindow.setPosition(evt.latLng);
                    infowindow.open(map);
                  };
                })(mph)
              );
            }
          }
        }
      }
    }

    setTimeout(() => {
      readyTo();
    }, 100);
  }, []);

  return (
    <div className="container-fluid" style={{ paddingTop: '100px'}}>
      <div className="row">
        <div className="col-12 col-md-6 mb-2">
          <div
            id="map"
            style={{ width: "100%", height: "700px" }}
            ref={mapRef}
          ></div>
        </div>
        <div className="col-12 col-md-6 mb-2" style={{maxHeight: '700px', overflowY: 'scroll'}}>
            <div id="panel" ></div>
        </div>
      </div>
    </div>
  );
}
