import Breadcrumbs from "../../components/Breadcrumbs";
import Router from "next/router";
import useTranslation from "next-translate/useTranslation";
import { useEffect } from "react";
import axios from "axios";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { getToken } from "../../features/user/userSlice";
import Link from "next/link";
import Head from "next/head";

const ResetPassword = () => {
  let { t } = useTranslation();
  let accountId;
  if (typeof window !== "undefined") {
    accountId = localStorage.getItem("accountId");
  }

  useEffect(() => {
    if (accountId) {
      Router.push("/");
    }
  });

  const dispatch = useDispatch();
  let loading = useSelector((state) => state.user.loading);
  let errorMessage = useSelector((state) => state.user.errorMessage);

  let submitHandler = (values) => {
    dispatch(getToken(values));
    Router.push("/");
  };
  return (
    <>
      <Head>
        <title>{t("common:resetPassword")}</title>
      </Head>
      <Breadcrumbs title={t("common:resetPassword")} />
      <div className="container-fluid p-5 m-0 form_bg">
        <Formik
          initialValues={{ email: "", password: "" }}
          validate={(values) => {
            const errors = {};
            if (!values.email) {
              errors.email = t("common:emailRequired");
            } else if (
              !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
            ) {
              errors.email = t("common:emailIncomplete");
            }
            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            console.log(values, "values");
            submitHandler(values);
            setSubmitting(false);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            isValid,
            dirty,
          }) => {
            return (
              <form
                onSubmit={handleSubmit}
                className="col-12 col-md-6 p-3 mx-auto border shadow rounded bg-light"
              >
                <div className="form-group">
                  <small>
                    <label htmlFor="ForgotPasswordEmail">
                      {t("common:yourEmail")}
                    </label>
                  </small>
                  <input
                    onChange={handleChange}
                    defaultValue={values.email}
                    name="email"
                    type="email"
                    className="form-control"
                    id="ForgotPasswordEmail"
                    aria-describedby="emailHelp"
                    placeholder="Email"
                  />
                  <p className="text-danger">
                    {errors.email && touched.email && errors.email}
                  </p>
                </div>
                <div className="text-end mt-4">
                  <button type="submit" className="btn btn-danger">
                    {t("common:restore")}
                    {loading && (
                      <i className="fa-solid fa-spinner fa-spin-pulse"></i>
                    )}
                  </button>
                </div>
                <p>{errorMessage && errorMessage}</p>
              </form>
            );
          }}
        </Formik>
      </div>
    </>
  );
};
export default ResetPassword;
