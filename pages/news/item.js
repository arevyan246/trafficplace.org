import Image from "next/image";
import Link from "next/link";
import useTranslation from "next-translate/useTranslation";
import { useSelector } from "react-redux";
import apiUrl from "../../configs/api-url";
import Breadcrumbs from "../../components/Breadcrumbs";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import axios from "axios";

const Announcement = () => {
  const language = useSelector((state) => state.language.language);
  let { t } = useTranslation();
  const [data, setData] = useState(null);
  const router = useRouter();
  const myLoader = ({ src, width, quality }) => {
    return `https://admin.trafficplace.org/storage/${src}?w=${width}&q=${
      quality || 75
    }`;
  };

  const {
    query: { result, slug },
  } = router;

  useEffect(() => {
    axios
      .get(apiUrl.urlv + result + "/" + slug)
      .then((res) => res)
      .then(({ data }) => {
        setData(data.message);
      });
  }, []);

  return (
    <>
      <Breadcrumbs title={t("common:news")} />
      <div className="container">
        <div className="list-group w-100 pb-3">
          <div className="p-5 mb-4 mt-4 bg-light rounded-3">
            <div className="container-fluid">
              {data && (
                <div className="w-100">
                  {item.image && (
                    <Image
                      src={data.image}
                      loader={myLoader}
                      width={1200}
                      height={531}
                      className="img-responsive"
                      alt={data["seo_title_" + language]}
                    />
                  )}
                </div>
              )}
              <h1 className="display-5 fw-bold">
                {data && data["title_" + language]}
              </h1>
              <p className="col-md-8 fs-4">
                {data && data["description_" + language]}
              </p>
              <p className="col-md-8 fs-4">
                {data && data["excerpt_" + language]}
              </p>
              {data && (
                <small className="text-nowrap me-2">
                  <Link href={data.youtube_link}>
                    <a target="_blank">
                      <i className="fa-brands fa-youtube text-danger"></i>
                      <span className="ms-1 fw-bold">YouTube</span>
                    </a>
                  </Link>
                </small>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Announcement;
