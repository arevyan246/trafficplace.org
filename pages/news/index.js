import styles from "./News.module.css";
import Image from "next/image";
import Link from "next/link";
import Breadcrumbs from "../../components/Breadcrumbs";
import useTranslation from "next-translate/useTranslation";
import { useSelector } from "react-redux";
import apiUrl from "../../configs/api-url";
import Head from "next/head";

const News = ({ data }) => {
  let { t } = useTranslation();
  const language = useSelector((state) => state.language.language);
  const myLoader = ({ src, width, quality }) => {
    return `https://admin.trafficplace.org/storage/${src}?w=${width}&q=${
      quality || 75
    }`;
  };

  return (
    <>
    <Head>
      <title>{t("common:news")}</title>
    </Head>
      <Breadcrumbs title={t("common:news")} />
      <div className="container">
        <div className="row">
          {data &&
            data.message.map((item) => {
              return (
                <div
                  key={item.id}
                  className={`${styles.fadeInUp} col-lg-4 col-md-6 wow mt-3`}
                  data-wow-delay="0ms"
                >
                  <div className={`${styles.newsItem} ${styles.news_item}`}>
                    <div className="news_box">
                      <div className={styles.newsimg}>
                        {item.image && (
                          <Image
                            src={item.image}
                            loader={myLoader}
                            width={1200}
                            height={531}
                            className={`${styles.new_img} img-responsive`}
                            alt={item["title_" + language]}
                          />
                        )}
                      </div>
                      <div className={styles.news_content}>
                        <div className={styles.news_postdate}>
                          <span>{item.created_at.slice(0, 10)}</span>
                        </div>
                        <h3>{item["title_" + language]}</h3>
                        <div className={styles.news_text}>
                          <p>{item["description_" + language]}</p>
                        </div>
                        {item.youtube_link && (
                          <div className={styles.news_authorinfo}>
                            <span>
                              <Link href={item.youtube_link}>
                                <a target="_blank">
                                  <i className="fa-brands fa-youtube text-danger"></i>
                                  <span className="ms-1 fw-bold">YouTube</span>
                                </a>
                              </Link>
                            </span>
                          </div>
                        )}
                        {item.slug && (
                          <small className="text-nowrap">
                            <Link
                              href={{
                                pathname: "/news/item",
                                query: {
                                  result: data.result.className,
                                  slug: item.slug,
                                },
                              }}
                            >
                              <a className="text-secondary">
                                <span className="ms-1 fw-bold">
                                  {t("common:learnMore")}
                                </span>
                                <i className="fa-solid fa-angles-right text-success"></i>
                              </a>
                            </Link>
                          </small>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}

          {/* <div className="col-12 text-center mt-3 mb-3">
            <button className="btn btn-danger">{t("common:showMore")}</button>
          </div> */}
        </div>
      </div>
    </>
  );
};

export async function getStaticProps() {
  const res = await fetch(apiUrl.news);
  const json = await res.json();
  return {
    props: {
      data: json,
    },
  };
}

export default News;
