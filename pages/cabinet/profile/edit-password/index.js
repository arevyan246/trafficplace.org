import axios from "axios";
import React, { useState } from "react";
import Breadcrumbs from "../../../../components/Breadcrumbs";
import LeftMenu from "../../../../components/LeftMenu";
import styles from "./EditPassword.module.css";
import useTranslation from "next-translate/useTranslation";
import Head from "next/head";

const EditPassword = () => {
  let { t } = useTranslation();
  // const [inputValue, setInputValue] = useState({
  //   currentPassword: '',
  //   newPassword: '',
  //   confirmPassword: '',

  // })

  // const onChangeHandler = (event) => {
  //   event.preventDefault()
  //   setInputValue({
  //     ...inputValue,
  //     [event.target.name]: event.target.value
  //   })
  // }
  // const submitHandler = (event) => {
  //   event.preventDefault()
  //   axios.post('url', {
  //     surName: inputValue.currentPassword,
  //     surName: inputValue.newPassword,
  //     surName: inputValue.confirmPassword,

  //   }).then(res => console.log(res))
  //     .catch(e => console.log(e.message))
  // }

  return (
    <>
    <Head>
      <title>{t("common:changePassword")}</title>
    </Head>
      <Breadcrumbs title={t("common:changePassword")} />
      <div className="container-fluid">
        <div className="row flex-nowrap">
          <LeftMenu />
          <div className="col-10 py-3">
            <div className="row">
              <div className="col-md-8 col-12">
                {/* <form onSubmit={submitHandler}> */}
                <form>
                  <div
                    className={`${styles.edit_profile_container} shadow border rounded`}
                  >
                    <h3>{t("common:changePassword")}</h3>
                    <div className="mb-3">
                      <label htmlFor="currentPassword" className="form-label">
                        {t("common:currentPassword")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        type="text"
                        name="currentPassword"
                        // onChange={event => onChangeHandler(event)}
                        // defaultValue={inputValue.currentPassword}
                        className="form-control"
                        id="currentPassword"
                        placeholder={t("common:currentPassword")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="newassword" className="form-label">
                        {t("common:newPassword")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        type="text"
                        name="newassword"
                        // onChange={event => onChangeHandler(event)}
                        // defaultValue={inputValue.newassword}
                        className="form-control"
                        id="newassword"
                        placeholder={t("common:newPassword")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="confirmPassword" className="form-label">
                        {t("common:confirmPassword")}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        type="text"
                        name="confirmPassword"
                        // onChange={event => onChangeHandler(event)}
                        // defaultValue={inputValue.confirmPassword}
                        className="form-control"
                        id="confirmPassword"
                        placeholder={t("common:confirmPassword")}
                        required
                      />
                    </div>
                    <div className="col-12 text-end">
                      <button type="button" className="btn btn-outline-primary">
                        {t("common:change")}
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EditPassword;
