import axios from "axios";
import React, { useEffect, useState } from "react";
import Breadcrumbs from "../../../../components/Breadcrumbs";
import LeftMenu from "../../../../components/LeftMenu";
import styles from "./EditProfile.module.css";
import useTranslation from "next-translate/useTranslation";
import apiUrl from "../../../../configs/api-url";
import SuccessModal from "../../../../components/Modal";
import Head from "next/head";

const EditProfile = () => {
  let { t } = useTranslation();
  let userToken;
  let message;
  const [data, setData] = useState('');
  const [inputValue, setInputValue] = useState({
    surName: '',
    name: '',
    nameMiddle: '',
    birthDate: '',
    city: '',
    skype: '',
    telegram: '',
    instagram: '',
    facebook: '',
    linkedin: '',
    pinterest: '',
    phoneNumber: '',
    gender: ''

  })
  const [selectedImage, setSelectedImage] = useState()
  const [isSuccess, setIsSuccess] = useState(false)
  const [selectedPhones, setSelectedPhones] = useState({
    Whatsapp: inputValue?.phone?.length && inputValue?.phone[0]?.whats_up_viber === 1 && inputValue?.phone[0]?.whats_up_viber === 3 ? true : false,
    Viber: inputValue?.phone?.length && inputValue?.phone[0]?.whats_up_viber === 2 && inputValue?.phone[0]?.whats_up_viber === 3 ? true : false,
  });
  

  const onGenderChange = (event) => {
    setInputValue({
      ...inputValue,
      ['gender']:event.target.value
    })
  }

  
  const onPhoneChange = (event) => {
    console.log(event.target.checked)
    let selectedName = event.target.name;
    setSelectedPhones({
      ...selectedPhones,
      [selectedName]: event.target.checked,
    });

    

  };

  console.log(selectedPhones,'selectedPhones')

  const imageChangeHandler = (evt) => {
    const [file] = evt.target.files
    setInputValue({
      ...inputValue,
      // avatar:URL.createObjectURL(file)
      avatar:file
    })
  }

  const onChangeHandler = (event) => {
    event.preventDefault()
    let phoneInp = {
      phone:[
        {
          ...inputValue?.phone[0],
          phone:event.target.value
        }
      ]
    }
    if(event.target.name === 'phone') {
      setInputValue({
        ...inputValue,
        ['phone']:phoneInp.phone,
      })
    }else {
      setInputValue({
        ...inputValue,
         [event.target.name]: event.target.value
      })
    }
 
  }
  const submitHandler = (event) => {
    event.preventDefault()

    let socialPhoneCount = [selectedPhones].map((elem) => {
      let count;
      if (elem.Whatsapp) {
        count = 1;
      }
      if (elem.Viber) {
        count = 2;
      }
      if (elem.Whatsapp && elem.Viber) {
        count = 3;
      }
      if (!elem.Whatsapp && !elem.Viber) {
        count = 0;
      }
      return count;
    });

    console.log(socialPhoneCount,'socialPhoneCount')
    
     let obj = {
      ...inputValue,
     }
     obj.phone[0].whats_up_viber = socialPhoneCount.toString()
     console.log(obj)
    axios.put(apiUrl.account, {
      ...obj,
    },{
      headers: {
        Authorization: "bearer " + userToken,
      }
    }).then(res => setIsSuccess(pre => !pre))
      .catch(e => console.log(e.message))
  }

  // let fm = new FormData();
  // fm.append('avatar',inputValue.avatar);
  // axios({
  //   method:'put',
  //   url:apiUrl.account,
  //   data:fm,
  //   headers:{
  //     Authorization: "bearer " + userToken,
  //     "Content-Type": "multipart/form-data"
  //   }
  //  }).then(res => setIsSuccess(pre => !pre))
  //  .catch(e => console.log(e.message))
  
  if (typeof window !== "undefined") {
    message = localStorage.getItem("message");
    userToken = localStorage.getItem("token");
    setTimeout(() => {
      localStorage.removeItem("message");
    }, 1000);
  }
  useEffect(() => {
    axios
      .get(apiUrl.me, {
        headers: {
          Authorization: "bearer " + userToken,
        },
      })
      .then((res) => setInputValue(res.data.message))
      .catch((e) => console.log(e.message));
  }, []);
  console.log(inputValue , "inp[utV")
  return (
    <>
    <Head>
      <title>{t("common:editProfile")}</title>
    </Head>
    {isSuccess && <SuccessModal massage={t("common:profileChanged")} />}
      <Breadcrumbs title={t("common:editProfile")} />
      <div className="container-fluid">
        <div className="row flex-nowrap">
          <LeftMenu />
          <div className="col-10 py-3">
            <div className="row">
              <div className="col-md-8 col-12">
                <form onSubmit={submitHandler}>
                  <div
                    className={`${styles.edit_profile_container} shadow border rounded`}
                  >
                    <h3>{t("common:userInformation")}</h3>
                    <div className="mb-3">
                      <label
                        htmlFor="registrationSurname"
                        className="form-label"
                      >
                        {t("common:name")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        type="text"
                        name="name"
                        onChange={event => onChangeHandler(event)}
                        defaultValue={inputValue.name}
                        className="form-control"
                        id="registrationSurname"
                        placeholder={t("common:name")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="registrationName" className="form-label">
                        {t("common:surname")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        onChange={event => onChangeHandler(event)}
                        type="text"
                        name="surname"
                        defaultValue={inputValue.surname}
                        className="form-control"
                        id="registrationName"
                        placeholder={t("common:surname")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <label
                        htmlFor="registrationNameMiddle"
                        className="form-label"
                      >
                        {t("common:nameMiddle")}
                      </label>
                      <input
                        onChange={event => onChangeHandler(event)}
                        name="lastname"
                        defaultValue={inputValue.lastname}
                        type="text"
                        className="form-control"
                        id="registrationNameMiddle"
                        placeholder={t("common:nameMiddle")}
                      />
                    </div>
                    <div className="mb-3">
                      <label
                        htmlFor="registrationCity"
                        className="form-label text-capitalize"
                      >
                        {t("common:city")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        onChange={event => onChangeHandler(event)}
                        type="text"
                        name="city"
                        defaultValue={inputValue.city}
                        className="form-control"
                        id="registrationCity"
                        placeholder={t("common:city")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="registrationDate" className="form-label">
                        {t("common:birthdate")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        onChange={event => onChangeHandler(event)}
                        type="date"
                        name="b_day"
                        defaultValue={inputValue.b_day}
                        className="form-control"
                        id="registrationDate"
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <div className="container">
                        <div className="row">
                          <label className="form-label">
                            {t("common:gender")}
                          </label>
                          <div className="col-6">
                            <div className="form-check">
                              <input
                                onChange={onGenderChange}
                                className="form-check-input"
                                type="radio"
                                name="gender"
                                id="man"
                                defaultValue="Male"
                                checked={inputValue.gender === "Male"}
                              />
                              <label className="form-check-label" htmlFor="man">
                              {t("common:man")}
                              </label>
                            </div>
                          </div>
                          <div className="col-6">
                            <div className="form-check">
                              <input
                                onChange={onGenderChange}
                                className="form-check-input"
                                type="radio"
                                checked={inputValue.gender === "Woman"}
                                name="gender"
                                defaultValue="Woman"
                                id="woman"
                              />
                              <label
                                className="form-check-label"
                                htmlFor="woman"
                              >
                                {t("common:woman")}
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="mb-3">
                      <label htmlFor="registrationCity" className="form-label">
                        {t("common:yourPhoto")}
                      </label>
                      <input
                        type="file"
                        name="avatar"
                        onChange={imageChangeHandler}
                        className="form-control"
                      />
                      <img src={inputValue.avatar} className='img-fluid' width={200} />
                    </div>
                  </div>

                  <div
                    className={`${styles.edit_profile_container} shadow border rounded mt-3`}
                  >
                    <div className="mb-3">
                      <label htmlFor="registrationTel" className="form-label">
                        {t("common:tel")} <span className="text-danger">*</span>
                      </label>
                      <input
                        type="tel"
                        name="phone"
                        defaultValue={inputValue?.phone?.length && inputValue?.phone[0]?.phone}
                        onChange={event => onChangeHandler(event)}
                        className="form-control"
                        id="registrationTel"
                        placeholder="123-45-678"
                        required
                      />
                      
                      <div className="container">
                          <div className="row">
                            <div className="col-6">
                              <div className="form-check form-check-inline">
                                <input
                                  onChange={onPhoneChange}
                                  className="form-check-input"
                                  type="checkbox"
                                  id="whatsapp"
                                  name="Whatsapp"
                                  defaultValue={'Whatsapp'}
                                  checked={
                                    selectedPhones.Whatsapp && "Whatsapp"
                                  }
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor="whatsapp"
                                >
                                  <i className="fa-brands fa-whatsapp me-1 text-success"></i>
                                  Whatsapp
                                </label>
                              </div>
                            </div>
                            <div className="col-6">
                              <div className="form-check form-check-inline">
                                <input
                                  onChange={onPhoneChange}
                                  className="form-check-input"
                                  type="checkbox"
                                   checked={selectedPhones.Viber && "Viber"}
                                  defaultValue="Viber"
                                  id="viber"
                                  name="Viber"
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor="viber"
                                >
                                  <i className="fa-brands fa-viber me-1 text-danger"></i>
                                  Viber
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div className="mb-3">
                      <label htmlFor="registrationSkype" className="form-label">
                        Skype
                      </label>
                      <input
                        name="skype"
                        defaultValue={inputValue.skype}
                        onChange={event => onChangeHandler(event)}
                        type="text"
                        className="form-control"
                        id="registrationSkype"
                      />
                    </div>
                    <div className="mb-3">
                      <label
                        htmlFor="registrationTelegram"
                        className="form-label"
                      >
                        Telegram
                      </label>
                      <input
                        name="telegram"
                        defaultValue={inputValue.telegram}
                        onChange={event => onChangeHandler(event)}
                        type="text"
                        className="form-control"
                        id="registrationTelegram"
                      />
                    </div>
                    
                  <div className="col-12 text-end">
                      <button type="submit" className="btn btn-outline-primary">
                        {t("common:change")}
                      </button>
                    </div>
                  </div>

                  {/* <div
                    className={`${styles.edit_profile_container} shadow border rounded mt-3`}
                  >
                    <h4>{t("common:socialNetworks")}</h4>
                    <div className="mb-3">
                      <label
                        htmlFor="registrationInstagram"
                        className="form-label"
                      >
                        <i className="fa-brands fa-instagram me-1"></i>
                        Instagram
                      </label>
                      <div className="form-control">
                        instagram.com/
                        <input
                          name="instagram"
                          defaultValue={inputValue.instagram}
                          onChange={event => onChangeHandler(event)}
                          type="text"
                          className="border-0"
                          id="registrationInstagram"
                          placeholder={t("common:name")}
                        />
                      </div>
                    </div>
                    <div className="mb-3">
                      <label
                        htmlFor="registrationFacebook"
                        className="form-label"
                      >
                        <i className="fa-brands fa-facebook me-1"></i>
                        Facebook
                      </label>
                      <div className="form-control">
                        facebook.com/
                        <input
                          name="facebook"
                          defaultValue={inputValue.facebook}
                          onChange={event => onChangeHandler(event)}
                          type="text"
                          className="border-0"
                          id="registrationFacebook"
                          placeholder={t("common:name")}
                        />
                      </div>
                    </div>
                    <div className="mb-3">
                      <label
                        htmlFor="registrationLinkedin"
                        className="form-label"
                      >
                        <i className="fa-brands fa-linkedin me-1"></i>
                        Linkedin
                      </label>
                      <div className="form-control">
                        linkedin.com/
                        <input
                          name="linkedin"
                          defaultValue={inputValue.linkedin}
                          onChange={event => onChangeHandler(event)}
                          type="text"
                          className="border-0"
                          id="registrationLinkedin"
                          placeholder={t("common:name")}
                        />
                      </div>
                    </div>
                    <div className="mb-3">
                      <label
                        htmlFor="registrationPinterest"
                        className="form-label"
                      >
                        <i className="fa-brands fa-pinterest me-1"></i>
                        Pinterest
                      </label>
                      <div className="form-control">
                        pinterest.com/
                        <input
                          name="pinterest"
                          defaultValue={inputValue.pinterest}
                          onChange={event => onChangeHandler(event)}
                          type="text"
                          className="border-0"
                          id="registrationPinterest"
                          placeholder={t("common:name")}
                        />
                      </div>
                    </div>
                    <div className="col-12 text-end">
                      <button type="submit" className="btn btn-outline-primary">
                        {t("common:change")}
                      </button>
                    </div>
                  </div> */}
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EditProfile;
