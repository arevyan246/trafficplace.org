import axios from "axios";
import React, { useState } from "react";
import Breadcrumbs from "../../../../components/Breadcrumbs";
import LeftMenu from "../../../../components/LeftMenu";
import styles from "./EditEmail.module.css";
import useTranslation from "next-translate/useTranslation";

const EditEmail = () => {
  let { t } = useTranslation();
  // const [inputValue, setInputValue] = useState({
  //   currentEmail: '',
  //   newEmail: '',

  // })

  // const onChangeHandler = (event) => {
  //   event.preventDefault()
  //   setInputValue({
  //     ...inputValue,
  //     [event.target.name]: event.target.value
  //   })
  // }
  // const submitHandler = (event) => {
  //   event.preventDefault()
  //   axios.post('url', {
  //     surName: inputValue.currentEmail,
  //     surName: inputValue.newEmail,

  //   }).then(res => console.log(res))
  //     .catch(e => console.log(e.message))
  // }

  return (
    <>
      <Breadcrumbs title={t("common:changeEmail")} />
      <div className="container-fluid">
        <div className="row flex-nowrap">
          <LeftMenu />
          <div className="col-10 py-3">
            <div className="row">
              <div className="col-md-8 col-12">
                {/* <form onSubmit={submitHandler}> */}
                <form>
                  <div
                    className={`${styles.edit_profile_container} shadow border rounded`}
                  >
                    <h3>{t("common:changeEmail")}</h3>
                    <div className="mb-3">
                      <label htmlFor="currentEmail" className="form-label">
                        {t("common:yourEmail")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        type="email"
                        name="currentEmail"
                        // onChange={event => onChangeHandler(event)}
                        // defaultValue={inputValue.currentEmail}
                        className="form-control"
                        id="currentEmail"
                        placeholder={t("common:yourEmail")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="newEmail" className="form-label">
                        {t("common:yourNewEmail")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        type="email"
                        name="newEmail"
                        // onChange={event => onChangeHandler(event)}
                        // defaultValue={inputValue.newEmail}
                        className="form-control"
                        id="newEmail"
                        placeholder={t("common:yourNewEmail")}
                        required
                      />
                    </div>
                    <div className="col-12 text-end">
                      <button type="button" className="btn btn-outline-primary">
                        {t("common:change")}
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EditEmail;
