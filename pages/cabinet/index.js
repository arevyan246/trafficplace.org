import Link from "next/link";
import useTranslation from "next-translate/useTranslation";
import Breadcrumbs from "../../components/Breadcrumbs";
import LeftMenu from "../../components/LeftMenu";
import Head from "next/head";
import axios from "axios";
import apiUrl from "../../configs/api-url";
import Router from "next/router";
import {useSelector} from "react-redux";


// const token = useSelector((state) => state.user.token);
const Cabinet = () => {
  let { t } = useTranslation();
    const token = useSelector((state) => state.user.token);
  console.log(token)
  axios
      .post(
          apiUrl.cabinet,
          {
          },
          {
            headers: {
              Authorization: "bearer " + token,
            },
          }
      )
      .then((res) => {
        console.log(e.res)
      })
      .catch((e) => console.log(e.message));
  let cabinetItems = [
    {
      id: 1,
      icon: "fa-truck",
      name: t("common:cars"),
      count: 12,
      url: '/cars'
    },
    {
      id: 2,
      icon: "fa-box",
      name: t("common:loads"),
      count: 30,
      url: '/loads'
    },
  ];
  
  return (
    <>
    <Head>
      <title>{t("common:myCabinet")}</title>
    </Head>
      <Breadcrumbs title={t("common:myCabinet")} />
      <div className="container-fluid">
        <div className="row flex-nowrap">
          <LeftMenu />
          <div className="col-10 py-3 marketing">
            <div className="row text-center">
              {cabinetItems.map((item) => {
                return (
                  <div key={item.id} className="col-12 col-md-6 mb-3">
                    <div
                      className="border m-auto p-3 shadow rounded"
                      style={{ width: 230 }}
                    >
                      <div
                        className="bg-danger rounded-circle shadow m-auto d-flex justify-content-center align-items-center"
                        style={{ width: 130, height: 130 }}
                      >
                        <i
                          className={`fa-sharp fa-solid ${item.icon} fa-4x text-white`}
                        ></i>
                      </div>
                      <h2 className="m-2">{item.name}</h2>
                      <h4>{item.count}</h4>
                      <a className="btn btn-primary" href={item.url} role="button">
                        {t("common:learnMore")}
                      </a>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Cabinet;
