import React, { useState, useEffect } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import Breadcrumbs from "../../../components/Breadcrumbs";
import LeftMenu from "../../../components/LeftMenu";
import Router from "next/router";
import styles from "./AddCar.module.css";
import apiUrl from "../../../configs/api-url";
import SuccessModal from "../../../components/Modal";
import useTranslation from "next-translate/useTranslation";
import Head from "next/head";

const AddCar = ({ cars }) => {
  let { t } = useTranslation();
  const token = useSelector((state) => state.user.token);
  const [data, setData] = useState(null);
  const [selectCargoType, setSelectCargoType] = useState();
  const [selectLoadingType, setSelectLoadingType] = useState();
  const [selectDocPermission, setSelectDocPermission] = useState();
  const language = useSelector((state) => state.language.language);
  let message;
  let accountId;

  if (typeof window !== "undefined") {
    message = localStorage.getItem("message");
    accountId = localStorage.getItem("accountId");
    setTimeout(() => {
      localStorage.removeItem("message");
    }, 1000);
  }

  const [inputValue, setInputValue] = useState({
    from: "",
    destination: "",
    date_from: "",
    date_to: "",
    trans_type_id: "",
    count_trucks: "",
    truck_body_id: "",
    pallet: "",
    weight_capacity: "",
    volume: "",
    truck_dimensions_length: "",
    truck_dimensions_width: "",
    truck_dimensions_height: "",
    offer: "",
    payment_type: "",
    from_lat: "",
    from_lon: "",
    to_lat: "",
    to_lon: "",
  });

  
  const cargoType = (event) => {
    setSelectCargoType({
      ...selectCargoType,
      [event.target.name]: event.target.checked,
    });
  };

  const loadingType = (event) => {
    setSelectLoadingType({
      ...selectLoadingType,
      [event.target.name]: event.target.checked,
    });
  };

  const docPermission = (event) => {
    setSelectDocPermission({
      ...selectDocPermission,
      [event.target.name]: event.target.checked,
    });
  };

  const onChangeHandler = (event) => {
    event.preventDefault();
    setInputValue({
      ...inputValue,
      [event.target.name]: event.target.value,
    });
  };

  const submitHandler = (event) => {
    event.preventDefault();
    let onCargoTypeChange = [selectCargoType].map((elem) => {
      let count = [];
      if (elem[1]) {
        count.push(1);
      }
      if (elem[2]) {
        count.push(2);
      }
      if (elem[3]) {
        count.push(3);
      }
      if (elem[4]) {
        count.push(4);
      }
      return count;
    });

    let loadingCount = [selectLoadingType].map((elem) => {
      let count = [];
      if (elem[1]) {
        count.push(1);
      }
      if (elem[2]) {
        count.push(2);
      }
      if (elem[3]) {
        count.push(3);
      }
      return count;
    });

    let docPermissionCount = [selectDocPermission].map((elem) => {
      let count = [];
      if (elem.TIR) {
        count.push(1);
      }
      if (elem.T1) {
        count.push(2);
      }
      if (elem.CMR) {
        count.push(3);
      }
      if (elem.Санпаспорт) {
        count.push(4);
      }
      if (elem.ADR) {
        count.push(5);
      }
      return count;
    });

    axios
      .post(
        apiUrl.addCar,
        {
          from: inputValue.from,
          to: inputValue.destination,
          date_from: inputValue.date_from,
          date_to: inputValue.date_to,
          count_trucks: inputValue.count_trucks,
          truck_body_id: inputValue.truck_body_id,
          pallet: inputValue.pallet,
          weight_capacity: inputValue.weight_capacity,
          volume: inputValue.volume,
          trans_type_id: inputValue.trans_type_id,
          truck_dimensions_length: inputValue.truck_dimensions_length,
          truck_dimensions_width: inputValue.truck_dimensions_width,
          truck_dimensions_height: inputValue.truck_dimensions_height,
          offer: inputValue.offer,
          payment_type: inputValue.payment_type,

          from_lat: inputValue.from_lat,
          from_lon: inputValue.from_lon,
          to_lat: inputValue.to_lat,
          to_lon: inputValue.to_lon,

          cargo_type: onCargoTypeChange[0],
          loading_type: loadingCount[0],
          doc_permission: docPermissionCount[0],
        },
        {
          headers: {
            Authorization: "bearer " + token,
          },
        }
      )
      .then((res) => {
        if (typeof window !== "undefined") {
          localStorage.setItem("message", "succsess");
        }
        Router.push("/cars");
      })
      .catch((e) => console.log(e.message));
  };

  const getDate = () => {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, "0");
    let mm = String(today.getMonth() + 1).padStart(2, "0");
    let yyyy = today.getFullYear();

    return (today = yyyy + "-" + mm + "-" + dd + "T00:00");
  };

  useEffect(() => {
    if(!google){
      Router.push("/cars");
    }
    if (!accountId) {
      Router.push("/login");
    }
  },[]);

  useEffect(() => {
    function readyTo() {
      let autocomplete;
      let autocomplete1;
      let id = 'from';
      let id1 = 'destination';

      autocomplete = new google.maps.places.Autocomplete((document.querySelector('#' + id)), {
        types: ['geocode']
      })

      autocomplete1 = new google.maps.places.Autocomplete((document.querySelector('#' + id1)), {
        types: ['geocode']
      })

      google.maps.event.addListener(autocomplete, 'place_changed', function () {
        let place = autocomplete.getPlace();
        console.log(place , "inputValue.from_lat");
        setInputValue({
          ...inputValue,
          from_lat:place.geometry.location.lat(),
          from_lon:place.geometry.location.lng(),
          from:place.name,
        })      
      })

      google.maps.event.addListener(autocomplete1, 'place_changed', function () {
        let place = autocomplete1.getPlace();
        setInputValue({
          ...inputValue,
          to_lat:place.geometry.location.lat(),
          to_lon:place.geometry.location.lng(),
          destination:place.name,

        })      

      })
    }
    readyTo()
  }, [inputValue])

  useEffect(() => {
    axios
      .get(apiUrl.CarShowQueries, {})
      .then((res) => setData(res.data.message))
      .catch((e) => console.log(e.message));
  }, []);
  
  return (
    <>
    <Head>
      <title>{t("common:addCar")}</title>
    </Head>
      <Breadcrumbs title={t("common:addCar")} />
      {message && (
        <SuccessModal massage={t("common:successfullyRegistration")} />
      )}
      <div className="container-fluid">
        <div className="row flex-nowrap">
          <LeftMenu />
          <div className="col-10 py-3">
            <div className="row">
              <div className="col-md-8 col-12">
                <form onSubmit={submitHandler}>
                  <div
                    className={`${styles.addCar_container} shadow border rounded`}
                  >
                    <h3>{t("common:route")}</h3>
                    <small className="form-text text-muted">
                      {t("common:routeText")}
                    </small>
                    <div className="mb-3">
                      <label htmlFor="from" className="form-label">
                        {t("common:fromWhere")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        type="text"
                        name="from"
                        onChange={(event) => onChangeHandler(event)}
                        defaultValue={inputValue.from}
                        className={`${styles.from_terr} form-control`}
                        id="from"
                        placeholder={t("common:city")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <label
                        htmlFor="destination"
                        className="form-label text-capitalize"
                      >
                        {t("common:where")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        type="text"
                        name="destination"
                        onChange={(event) => onChangeHandler(event)}
                        defaultValue={inputValue.destination}
                        className={`${styles.to_terr} form-control`}
                        id="destination"
                        placeholder={t("common:cityOrCounty")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="date_from" className="form-label">
                        {t("common:uploadDate")}
                        <span className="text-danger">*</span>
                      </label>
                      <div className="d-flex align-items-center">
                        <input
                          onChange={(event) => onChangeHandler(event)}
                          type="datetime-local"
                          min={getDate()}
                          name="date_from"
                          defaultValue={inputValue.date_from}
                          className="form-control"
                          id="date_from"
                          required
                        />
                        <p className="ms-1 me-1 m-0">-</p>
                        <input
                          onChange={(event) => onChangeHandler(event)}
                          type="datetime-local"
                          min={getDate()}
                          name="date_to"
                          defaultValue={inputValue.date_to}
                          className="form-control"
                          required
                        />
                      </div>
                    </div>
                  </div>

                  <div
                    className={`${styles.addCar_container} shadow border rounded mt-3`}
                  >
                    <h3>{t("common:carDetails")}</h3>
                    <small className="form-text text-muted">
                      {t("common:carDetails")}
                    </small>
                    <div className="mb-3">
                      <label htmlFor="trans_type_id" className="form-label">
                        {t("common:car")}
                        <span className="text-danger">*</span>
                      </label>
                      <select
                        className="form-control"
                        name="trans_type_id"
                        id="trans_type_id"
                        onChange={(event) => onChangeHandler(event)}
                        required
                      >
                        <option value="1">полуприцеп</option>
                        <option value="2">грузовик</option>
                        <option value="3">сцепка</option>
                      </select>
                    </div>
                    <div className="mb-3">
                      <label htmlFor="count_trucks" className="form-label">
                        {t("common:countCar")}
                      </label>
                      <input
                        type="number"
                        min="0"
                        name="count_trucks"
                        onChange={(event) => onChangeHandler(event)}
                        defaultValue={inputValue.count_trucks}
                        className="form-control"
                        id="count_trucks"
                        placeholder="0"
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="truck_body_id" className="form-label">
                        {t("common:body")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <div className="row">
                        <div className="col-12 col-md-9">
                          <div className="form-group">
                            <select
                              className="form-control"
                              name="truck_body_id"
                              id="truck_body_id"
                              onChange={(event) => onChangeHandler(event)}
                              required
                            >
                              <option value="1">выберите кузов</option>
                              <option value="2">тент</option>
                              <option value="3">цельнометал</option>
                              <option value="4">изотерм</option>
                              <option value="5">рефрижератор</option>
                            </select>
                          </div>
                        </div>
                        <div className="col-6 col-md-3">
                          <input
                            type="number"
                            min="0"
                            name="pallet"
                            onChange={(event) => onChangeHandler(event)}
                            defaultValue={inputValue.pallet}
                            className="form-control"
                            id="pallet"
                            placeholder={t("common:pallets")}
                            required
                          />
                        </div>
                      </div>
                    </div>
                    <div className="mb-3">
                      <label htmlFor="weight_capacity" className="form-label">
                        {t("common:carryingCapacity")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        type="number"
                        min="0"
                        name="weight_capacity"
                        onChange={(event) => onChangeHandler(event)}
                        defaultValue={inputValue.weight_capacity}
                        className="form-control"
                        id="weight_capacity"
                        placeholder={t("common:tons")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="volume" className="form-label">
                        {t("common:volume")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        type="number"
                        min="0"
                        name="volume"
                        onChange={(event) => onChangeHandler(event)}
                        defaultValue={inputValue.volume}
                        className="form-control"
                        id="volume"
                        placeholder={t("common:cubeM")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <div className="container">
                        <div className="row">
                          <label className="form-label">
                            {t("common:loading")}
                          </label>
                          <small className="form-text text-muted">
                            {t("common:loadingText")}
                          </small>
                          {data &&
                            data.loadingtype.map((e) => {
                              return (
                                <div key={e.id} className="col-12 col-md-4">
                                  <div className="form-check">
                                    <input
                                      onChange={loadingType}
                                      className="form-check-input"
                                      type="checkbox"
                                      name={e.id}
                                      id="one"
                                      defaultValue="Behind"
                                    />
                                    <label
                                      className="form-check-label"
                                      htmlFor="behind"
                                    >
                                      {e["name_" + language]}
                                    </label>
                                  </div>
                                </div>
                              );
                            })}
                        </div>
                      </div>
                    </div>
                    <div className="mb-3">
                      <label
                        htmlFor="registrationSurname"
                        className="form-label"
                      >
                        {t("common:bodyDimensions")}
                      </label>
                      <div className="row">
                        <div className="col-4">
                          <input
                            type="number"
                            min="0"
                            name="truck_dimensions_length"
                            onChange={(event) => onChangeHandler(event)}
                            defaultValue={inputValue.truck_dimensions_length}
                            className="form-control"
                            placeholder={t("common:lengthM")}
                            required
                          />
                        </div>
                        <div className="col-4">
                          <input
                            type="number"
                            min="0"
                            name="truck_dimensions_width"
                            onChange={(event) => onChangeHandler(event)}
                            defaultValue={inputValue.truck_dimensions_width}
                            className="form-control"
                            id="registrationSurname"
                            placeholder={t("common:widthM")}
                            required
                          />
                        </div>
                        <div className="col-4">
                          <input
                            type="number"
                            min="0"
                            name="truck_dimensions_height"
                            onChange={(event) => onChangeHandler(event)}
                            defaultValue={inputValue.truck_dimensions_height}
                            className="form-control"
                            placeholder={t("common:heightM")}
                            required
                          />
                        </div>
                        <div className="row mt-3">
                          {data &&
                            data.cargotype.map((e) => {
                              return (
                                <div key={e.id} className="col-12 col-md-3">
                                  <div className="form-check">
                                    <input
                                      onChange={cargoType}
                                      className="form-check-input"
                                      type="checkbox"
                                      name={e.id}
                                      id={e.id}
                                    />
                                    <label
                                      className="form-check-label"
                                      htmlFor={e.id}
                                    >
                                      {e["name_" + language]}
                                    </label>
                                  </div>
                                </div>
                              );
                            })}
                        </div>
                      </div>
                    </div>
                    <div className="mb-3">
                      <div className="row">
                        <label className="form-label">
                          {t("common:permissions")}
                        </label>
                        <small className="form-text text-muted">
                          {t("common:permissionsText")}
                        </small>
                        {data &&
                          data.docpermission.map((e) => {
                            return (
                              <div key={e.id} className="col-6 col-md-3">
                                <div className="form-check">
                                  <input
                                    onChange={docPermission}
                                    className="form-check-input"
                                    type="checkbox"
                                    name={e.name}
                                    id={e.name}
                                    defaultValue={e.name}
                                  />
                                  <label
                                    className="form-check-label"
                                    htmlFor={e.name}
                                  >
                                    {e.name}
                                  </label>
                                </div>
                              </div>
                            );
                          })}
                      </div>
                    </div>
                  </div>

                  <div
                    className={`${styles.addCar_container} shadow border rounded mt-3`}
                  >
                    <h3>{t("common:payment")}</h3>
                    <small className="form-text text-muted">
                      {t("common:paymentText")}
                    </small>
                    <div className="row">
                      <div className="col-8">
                        <label htmlFor="offer" className="form-label">
                          {t("common:offer")}
                        </label>
                        <input
                          type="number"
                          min="0"
                          name="offer"
                          className="form-control"
                          id="offer"
                          placeholder="0"
                          onChange={(event) => onChangeHandler(event)}
                          defaultValue={inputValue.offer}
                        />
                      </div>
                      <div className="col-4">
                        <label htmlFor="payment_type" className="form-label">
                          {t("common:paymentType")}
                        </label>
                        <select
                          className="form-control"
                          name="payment_type"
                          id="payment_type"
                          onChange={(event) => onChangeHandler(event)}
                          defaultValue={inputValue.payment_type}
                        >
                          <option value="0">{t("common:paymentType")}</option>
                          <option value="1">{t("common:cash")}</option>
                          <option value="2">{t("common:cashless")}</option>
                        </select>
                      </div>
                    </div>

                    <div className="col-12 mt-5 text-end">
                      <button type="submit" className="btn btn-outline-primary">
                        {t("common:add")}
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AddCar;
