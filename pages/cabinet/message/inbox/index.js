import axios from "axios";
import React, { useState } from "react";
import Image from "next/image";
import Breadcrumbs from "../../../../components/Breadcrumbs";
import LeftMenu from "../../../../components/LeftMenu";
import styles from "./Inbox.module.css";
import useTranslation from "next-translate/useTranslation";

const Inbox = () => {
  let { t } = useTranslation();

  return (
    <>
      <Breadcrumbs title={t("common:inbox")} />
      <div className="container-fluid">
        <div className="row flex-nowrap">
          <LeftMenu />
          <div className="col-10 py-3">
            <div className="row">
              <div className="col-md-8 col-12 m-auto">
                <div
                  className={`${styles.edit_profile_container} shadow border rounded`}
                >
                  <h3>{t("common:inbox")}</h3>
                  <div className="">
                    <div className="row border rounded">
                      <div className="p-2  col-auto d-flex align-items-center">
                        <Image
                          src="/no-avatar.svg"
                          // loader={myLoader}
                          width={50}
                          height={50}
                          className="img-responsive"
                          // alt={item["seo_title_" + language]}
                        />
                      </div>
                      <div className="p-2 col-auto">
                        <p className="mb-1">Name
                        {/* <span class="badge bg-success ms-2 rounded-pill">New</span> */}
                        </p>
                        <small>
                          messagess
                        </small>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Inbox;
