import Link from "next/link";
import { useState, useEffect } from "react";
import Breadcrumbs from "../../../components/Breadcrumbs";
import LeftMenu from "../../../components/LeftMenu";
import styles from "./Category.module.css";
import { useRouter } from "next/router";
import Router from "next/router";
import useTranslation from "next-translate/useTranslation";

const Category = () => {
  let { t } = useTranslation();
  let accountId;
  if (typeof window !== "undefined") {
    accountId = localStorage.getItem("accountId");
  }

  useEffect(() => {
    if (accountId !== 'null') {
      Router.push("/");
    }
  })

  const [regType, setRegType] = useState();
  const router = useRouter();
  useEffect(() => {
    if (typeof window !== "undefined") {
      localStorage.setItem("registered_type", regType);
    }
  }, [regType]);
  
  const urlHandler = (param) => {
    setRegType(param);
    router.push("/cabinet/registration");
  };
  return (
    <>
      <Breadcrumbs title={t("common:status")} />
      <div className="container-fluid">
        <div className="row flex-nowrap">
          <LeftMenu />
          <div className="col-10 py-3">
            <form>
              <div
                className={`${styles.category_container} shadow border rounded`}
              >
                <div className="row">
                  <div className="text-center w-100 mt-3 mb-3">
                    <h2>{t("common:statusTitle")}</h2>
                  </div>
                  <div className="col-md-5 col-sm-10 m-3 mx-auto">
                    <div className={`${styles.pricingTable} ${styles.green}`}>
                      <div className={styles.pricingTable_header}>
                        <i className="fa-sharp fa-solid fa-truck"></i>
                        <div className={styles.price_value}>
                          $20.00
                          {/* <span className={styles.month}>
                            Լրացուցիչ տեղեկություն
                            <p className="w-75 m-auto">
                              There is no one who loves pain itself, who seeks
                            </p>
                          </span> */}
                        </div>
                      </div>
                      <h3 className={styles.heading}>{t("common:carrier")}</h3>
                      <div className={styles.pricing_content}>
                        <ul className="text-start p-1">
                          <li>
                            <b>{t("common:announcements")}</b> ∞
                          </li>
                          <li>
                            <b>{t("common:work")}</b> ∞
                          </li>
                          <li>
                            <b>{t("common:loads")}</b> ∞
                          </li>
                          <li>
                            <b>{t("common:cars")}</b> ∞
                          </li>
                          <li>
                            <b>{t("common:resume")}</b> ∞
                          </li>
                          <li>
                            <b>{t("common:importingAnnouncements")}</b> ∞
                          </li>
                          <li>
                            <b>{t("common:importingWork")}</b> ∞
                          </li>
                        </ul>
                      </div>
                      <div
                        className={styles.pricingTable_signup}
                        onClick={() => urlHandler(1)}
                      >
                        <span>{t("common:continue")}</span>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-5 col-sm-10 m-3 mx-auto">
                    <div className={`${styles.pricingTable} ${styles.blue}`}>
                      <div className={styles.pricingTable_header}>
                        <i className="fa-solid fa-box"></i>
                        <div className={styles.price_value}>
                          $30.00
                          {/* <span className={styles.month}>
                            Լրացուցիչ տեղեկություն
                            <p className="w-75 m-auto">
                              There is no one who loves pain itself, who seeks
                            </p>
                          </span> */}
                        </div>
                      </div>
                      <h3 className={styles.heading}>{t("common:cargoOwner")}</h3>
                      <div className={styles.pricing_content}>
                        <ul className="text-start p-1">
                          <li>
                            <b>{t("common:announcements")}</b> 10
                          </li>
                          <li>
                            <b>{t("common:work")}</b> 0
                          </li>
                          <li>
                            <b>{t("common:loads")}</b> 3
                          </li>
                          <li>
                            <b>{t("common:cars")}</b> 2
                          </li>
                          <li>
                            <b>{t("common:resume")}</b> ∞
                          </li>
                          <li>
                            <b>{t("common:importingAnnouncements")}</b> ∞
                          </li>
                          <li>
                            <b>{t("common:importingWork")}</b> ∞
                          </li>
                        </ul>
                      </div>
                      <div
                        className={styles.pricingTable_signup}
                        onClick={() => urlHandler(2)}
                      >
                        <span>{t("common:continue")}</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default Category;
