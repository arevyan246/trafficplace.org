import axios from "axios";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Breadcrumbs from "../../../components/Breadcrumbs";
import LeftMenu from "../../../components/LeftMenu";
import styles from "./Registration.module.css";
import Router from "next/router";
import apiUrl from "../../../configs/api-url";
import useTranslation from "next-translate/useTranslation";

const Registration = () => {
  let { t } = useTranslation();
  let accountId;
  if (typeof window !== "undefined") {
    accountId = localStorage.getItem("accountId");
  }

  useEffect(() => {
    if (accountId !== "null") {
      Router.push("/");
    }
  });

  let registeredType;
  if (typeof window !== "undefined") {
    registeredType = localStorage.getItem("registered_type");
  }
  const [inputValue, setInputValue] = useState({
    surname: "",
    name: "",
    lastname: "",
    b_day: "",
    city: "",
    skype: "",
    telegram: "",
    registered_type: registeredType,
    instagram: "",
    facebook: "",
    phoneNumber: "",
  });
  const [selectGender, setSelectGender] = useState("Male");
  const [selectedImage, setSelectedImage] = useState();
  const [selectedPhones, setSelectedPhones] = useState({
    Whatsapp: "",
    Viber: "",
  });
  const token = useSelector((state) => state.user.token);

  const onGenderChange = (event) => {
    setSelectGender(event.target.value);
  };
  const onPhoneChange = (event) => {
    let selectedName = event.target.value;
    setSelectedPhones({
      ...selectedPhones,
      [selectedName]: event.target.checked,
    });
  };

  const imageChangeHandler = (evt) => {
    const [file] = evt.target.files;
    // setSelectedImage(file);
    // setSelectedImage(URL.createObjectURL(file))
    setSelectedImage(file)
  };

  const onChangeHandler = (event) => {
    event.preventDefault();
    setInputValue({
      ...inputValue,
      [event.target.name]: event.target.value,
    });
  };

  const submitHandler = (event) => {
    event.preventDefault();
    let socialPhoneCount = [selectedPhones].map((elem) => {
      let count;
      if (elem.Whatsapp) {
        count = 1;
      }
      if (elem.Viber) {
        count = 2;
      }
      if (elem.Whatsapp && elem.Viber) {
        count = 3;
      }
      if (!elem.Whatsapp && !elem.Viber) {
        count = 0;
      }
      return count;
    });

    axios
      .post(
        apiUrl.addAccount,
        {
          name: inputValue.name,
          surname: inputValue.surname,
          lastname: inputValue.lastname,
          city: inputValue.city,
          b_day: inputValue.b_day,
          skype: inputValue.skype,
          telegram: inputValue.telegram,
          registered_type: inputValue.registered_type,
          gender: selectGender,
          avatar: selectedImage,
          phones: [
            {
              phone: inputValue.phoneNumber,
              whats_up_viber: socialPhoneCount.toString(),
            },
          ],
          socialNetworks: [
            { name: "Instagram", link: inputValue.instagram },
            { name: "Facebook", link: inputValue.facebook },
          ],
        },
        {
          headers: {
            Authorization: "bearer " + token,
          },
        }
      )
      .then((res) => {
        if (registeredType === "1") {
          Router.push("/cabinet/add-car");
        } else if (registeredType === "2") {
          Router.push("/cabinet/add-load");
        }

        if (typeof window !== "undefined") {
          localStorage.setItem("accountId", res.data.result.account_id);
          localStorage.setItem("message", "succsess");
        }
      })
      .catch((e) => console.log(e.message));
  };
  return (
    <>
      <Breadcrumbs title={t("common:registration")} />
      <div className="container-fluid">
        <div className="row flex-nowrap">
          <LeftMenu />
          <div className="col-10 py-3">
            <div className="row">
              <div className="col-md-8 col-12">
                <form onSubmit={submitHandler}>
                  <input
                    type="hidden"
                    name="registered_type"
                    defaultValue={inputValue.registered_type}
                  />
                  <div
                    className={`${styles.registration_container} shadow border rounded`}
                  >
                    <h3>{t("common:userInformation")}</h3>
                    <div className="mb-3">
                      <label
                        htmlFor="registrationSurname"
                        className="form-label"
                      >
                        {t("common:surname")}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        type="text"
                        name="surname"
                        onChange={(event) => onChangeHandler(event)}
                        defaultValue={inputValue.surname}
                        className="form-control"
                        id="registrationSurname"
                        placeholder={t("common:surname")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="registrationName" className="form-label">
                        {t("common:name")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        onChange={(event) => onChangeHandler(event)}
                        type="text"
                        name="name"
                        defaultValue={inputValue.name}
                        className="form-control"
                        id="registrationName"
                        placeholder={t("common:name")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <label
                        htmlFor="registrationNameMiddle"
                        className="form-label"
                      >
                        {t("common:nameMiddle")}
                      </label>
                      <input
                        onChange={(event) => onChangeHandler(event)}
                        name="lastname"
                        defaultValue={inputValue.lastname}
                        type="text"
                        className="form-control"
                        id="registrationNameMiddle"
                        placeholder={t("common:nameMiddle")}
                      />
                    </div>
                    <div className="mb-3">
                      <label
                        htmlFor="registrationCity"
                        className="form-label text-capitalize"
                      >
                        {t("common:city")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        onChange={(event) => onChangeHandler(event)}
                        type="text"
                        name="city"
                        defaultValue={inputValue.city}
                        className="form-control"
                        id="registrationCity"
                        placeholder={t("common:city")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="registrationDate" className="form-label">
                        {t("common:birthdate")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        onChange={(event) => onChangeHandler(event)}
                        type="date"
                        name="b_day"
                        defaultValue={inputValue.b_day}
                        className="form-control"
                        id="registrationDate"
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <div className="container">
                        <div className="row">
                          <label className="form-label">{t("common:gender")}</label>
                          <div className="col-6">
                            <div className="form-check">
                              <input
                                onChange={onGenderChange}
                                className="form-check-input"
                                type="radio"
                                name="gender"
                                id="man"
                                defaultValue="Male"
                                checked={selectGender === "Male"}
                              />
                              <label className="form-check-label" htmlFor="man">
                              {t("common:man")}
                              </label>
                            </div>
                          </div>
                          <div className="col-6">
                            <div className="form-check">
                              <input
                                onChange={onGenderChange}
                                className="form-check-input"
                                type="radio"
                                checked={selectGender === "Woman"}
                                name="gender"
                                defaultValue="Woman"
                                id="woman"
                              />
                              <label
                                className="form-check-label"
                                htmlFor="woman"
                              >
                                {t("common:woman")}
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="mb-3">
                      <label htmlFor="registrationCity" className="form-label">
                      {t("common:yourPhoto")}
                      </label>
                      <input
                        type="file"
                        name="avatar"
                        onChange={imageChangeHandler}
                        className="form-control"
                      />
                    </div>
                  </div>

                  <div
                    className={`${styles.registration_container} shadow border rounded mt-3`}
                  >
                    <div className="mb-3">
                      <label htmlFor="registrationTel" className="form-label">
                      {t("common:tel")} <span className="text-danger">*</span>
                      </label>
                      <div className="mb-3">
                        <input
                          type="tel"
                          name="phoneNumber"
                          onChange={(event) => onChangeHandler(event)}
                          defaultValue={inputValue.phoneNumber}
                          className="form-control"
                          id="registrationTel"
                          placeholder="123-45-678"
                          required
                        />
                        <div className="container">
                          <div className="row">
                            <div className="col-6">
                              <div className="form-check form-check-inline">
                                <input
                                  onChange={onPhoneChange}
                                  className="form-check-input"
                                  type="checkbox"
                                  id="whatsapp"
                                  name="whatsapp"
                                  defaultValue="Whatsapp"
                                  // checked={
                                  //   selectedPhones.Whatsapp && "Whatsapp"
                                  // }
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor="whatsapp"
                                >
                                  <i className="fa-brands fa-whatsapp me-1 text-success"></i>
                                  Whatsapp
                                </label>
                              </div>
                            </div>
                            <div className="col-6">
                              <div className="form-check form-check-inline">
                                <input
                                  onChange={onPhoneChange}
                                  className="form-check-input"
                                  type="checkbox"
                                  // checked={selectedPhones.Viber && "Viber"}
                                  defaultValue="Viber"
                                  id="viber"
                                  name="Viber"
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor="viber"
                                >
                                  <i className="fa-brands fa-viber me-1 text-danger"></i>
                                  Viber
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="mb-3">
                      <label htmlFor="registrationSkype" className="form-label">
                        <i className="fa-brands fa-skype me-1 text-info"></i>
                        Skype
                      </label>
                      <input
                        name="skype"
                        defaultValue={inputValue.skype}
                        onChange={(event) => onChangeHandler(event)}
                        type="text"
                        className="form-control"
                        id="registrationSkype"
                      />
                    </div>
                    <div className="mb-3">
                      <label
                        htmlFor="registrationTelegram"
                        className="form-label"
                      >
                        <i className="fa-brands fa-telegram me-1 text-primary"></i>
                        Telegram
                      </label>
                      <input
                        name="telegram"
                        defaultValue={inputValue.telegram}
                        onChange={(event) => onChangeHandler(event)}
                        type="text"
                        className="form-control"
                        id="registrationTelegram"
                      />
                    </div>
                    <div className="col-12 text-end">
                      <button type="submit" className="btn btn-outline-primary">
                      {t('common:registration')}
                      </button>
                    </div>
                  </div>

                  {/* <div
                    className={`${styles.registration_container} shadow border rounded mt-3`}
                  >
                    <h4>{t("common:socialNetworks")}</h4>
                    <div className="mb-3">
                      <label
                        htmlFor="registrationInstagram"
                        className="form-label"
                      >
                        <i className="fa-brands fa-instagram me-1 text-danger"></i>
                        Instagram
                      </label>
                      <div className="form-control">
                        instagram.com/
                        <input
                          name="instagram"
                          defaultValue={inputValue.instagram}
                          onChange={(event) => onChangeHandler(event)}
                          type="text"
                          className="border-0"
                          id="registrationInstagram"
                          placeholder={t('common:name')}
                        />
                      </div>
                    </div>
                    <div className="mb-3">
                      <label
                        htmlFor="registrationFacebook"
                        className="form-label"
                      >
                        <i className="fa-brands fa-facebook me-1 text-primary"></i>
                        Facebook
                      </label>
                      <div className="form-control">
                        facebook.com/
                        <input
                          name="facebook"
                          defaultValue={inputValue.facebook}
                          onChange={(event) => onChangeHandler(event)}
                          type="text"
                          className="border-0"
                          id="registrationFacebook"
                          placeholder={t('common:name')}
                        />
                      </div>
                    </div>
                    <div className="col-12 text-end">
                      <button type="submit" className="btn btn-outline-primary">
                      {t('common:registration')}
                      </button>
                    </div>
                  </div> */}
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Registration;
