import axios from "axios";
import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Breadcrumbs from "../../../components/Breadcrumbs";
import LeftMenu from "../../../components/LeftMenu";
import Router from "next/router";
import styles from "./AddLoad.module.css";
import apiUrl from "../../../configs/api-url";
import SuccessModal from "../../../components/Modal";
import useTranslation from "next-translate/useTranslation";
import Head from "next/head";

const AddLoad = () => {
  let { t } = useTranslation();
  const token = useSelector((state) => state.user.token);
  const [selectRoundTrip, setSelectRoundTrip] = useState(false);
  const [selectLoadingType, setSelectLoadingType] = useState();
  const [selectDocPermission, setSelectDocPermission] = useState();
  const [data, setData] = useState(null);
  const language = useSelector((state) => state.language.language);
  let message;
  let accountId;

  if (typeof window !== "undefined") {
    message = localStorage.getItem("message");
    accountId = localStorage.getItem("accountId");
    setTimeout(() => {
      localStorage.removeItem("message");
    }, 1000);
  }

  useEffect(() => {
    if (!accountId) {
      Router.push("/login");
    }
  });

  const [inputValue, setInputValue] = useState({
    name: "",
    from: "",
    destination: "",
    loading_from: "",
    loading_to: "",
    packaging_type_id: "",
    weight: "",
    volume: "",
    cargo_type: "",
    count_trucks: "",
    truck_type_id: "",
    offer: "",
    payment_type: "",
    from_lat: "",
    from_lon: "",
    to_lat: "",
    to_lon: "",
  });

  const onRoundTripChange = () => {
    setSelectRoundTrip((prevCheck) => !prevCheck);
  };

  const loadingType = (event) => {
    setSelectLoadingType({
      ...selectLoadingType,
      [event.target.name]: event.target.checked,
    });
  };

  const docPermission = (event) => {
    setSelectDocPermission({
      ...selectDocPermission,
      [event.target.name]: event.target.checked,
    });
  };

  const onChangeHandler = (event) => {
    event.preventDefault();
    setInputValue({
      ...inputValue,
      [event.target.name]: event.target.value,
    });
  };

  const submitHandler = (event) => {
    event.preventDefault();
    let loadingCount = [selectLoadingType].map((elem) => {
      let count = [];
      if (elem[1]) {
        count.push(1);
      }
      if (elem[2]) {
        count.push(2);
      }
      if (elem[3]) {
        count.push(3);
      }
      return count;
    });

    let docPermissionCount = [selectDocPermission].map((elem) => {
      let count = [];
      if (elem.tir) {
        count.push(1);
      }
      if (elem.t1) {
        count.push(2);
      }
      if (elem.cmr) {
        count.push(3);
      }
      if (elem.adr) {
        count.push(4);
      }
      return count;
    });

    axios
      .post(
        apiUrl.addLoad,
        {
          name: inputValue.name,
          from: inputValue.from,
          to: inputValue.destination,
          date_from: inputValue.loading_from,
          date_to: inputValue.loading_to,
          packaging_type_id: inputValue.packaging_type_id,
          weight: inputValue.weight,
          volume: inputValue.volume,
          cargo_type: inputValue.cargo_type,
          count_trucks: inputValue.count_trucks,
          truck_type_id: inputValue.truck_type_id,
          round_trip: selectRoundTrip,
          offer: inputValue.offer,
          payment_type: inputValue.payment_type,

          from_lat: inputValue.from_lat,
          from_lon: inputValue.from_lon,
          to_lat: inputValue.to_lat,
          to_lon: inputValue.to_lon,

          loading_type: loadingCount[0],
          doc_permission: docPermissionCount[0],
        },
        {
          headers: {
            Authorization: "bearer " + token,
          },
        }
      )
      .then((res) => {
        if (typeof window !== "undefined") {
          localStorage.setItem("message", "succsess");
        }
        Router.push("/loads");
        console.log(res);
      })
      .catch((e) => console.log(e.message));
  };

  const getDate = () => {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, "0");
    let mm = String(today.getMonth() + 1).padStart(2, "0");
    let yyyy = today.getFullYear();

    return (today = yyyy + "-" + mm + "-" + dd + "T00:00");
  };

  useEffect(() => {
    function readyTo() {
      let autocomplete;
      let autocomplete1;
      let id = 'from';
      let id1 = 'destination';

      autocomplete = new google.maps.places.Autocomplete((document.querySelector('#' + id)), {
        types: ['geocode']
      })

      autocomplete1 = new google.maps.places.Autocomplete((document.querySelector('#' + id1)), {
        types: ['geocode']
      })

      google.maps.event.addListener(autocomplete, 'place_changed', function () {
        let place = autocomplete.getPlace();
        console.log(place , "inputValue.from_lat");
        setInputValue({
          ...inputValue,
          from_lat:place.geometry.location.lat(),
          from_lon:place.geometry.location.lng(),
          from:place.name,
        })      
      })

      google.maps.event.addListener(autocomplete1, 'place_changed', function () {
        let place = autocomplete1.getPlace();
        setInputValue({
          ...inputValue,
          to_lat:place.geometry.location.lat(),
          to_lon:place.geometry.location.lng(),
          destination:place.name,

        })      

      })
    }
    readyTo()
  }, [inputValue])

  useEffect(() => {
    axios
      .get(apiUrl.LaodShowQueries, {})
      .then((res) => setData(res.data.message))
      .catch((e) => console.log(e.message));
  }, []);
  console.log(inputValue, 'inputValue')

  return (
    <>
    <Head>
      <title>{t("common:addLoad")}</title>
    </Head>
      <Breadcrumbs title={t("common:addLoad")} />
      {message && (
        <SuccessModal massage="Դուք հաջողությամբ ավարտեցիք գրանցումը" />
      )}
      <div className="container-fluid">
        <div className="row flex-nowrap">
          <LeftMenu />
          <div className="col-10 py-3">
            <div className="row">
              <div className="col-md-8 col-12">
                <form onSubmit={submitHandler}>
                  <div
                    className={`${styles.addCar_container} shadow border rounded`}
                  >
                    <h3>{t("common:route")}</h3>
                    <small className="form-text text-muted">
                      {t("common:routeText")}
                    </small>
                    <div className="mb-3">
                      <label htmlFor="from" className="form-label">
                        {t("common:fromWhere")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        type="text"
                        name="from"
                        onChange={(event) => onChangeHandler(event)}
                        defaultValue={inputValue.from}
                        className={`${styles.from_terr} form-control`}
                        id="from"
                        placeholder={t("common:city")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <div className="row">
                        <div className="col-12 col-md-8">
                          <label htmlFor="destination" className="form-label">
                            {t("common:destination")}{" "}
                            <span className="text-danger">*</span>
                          </label>
                          <input
                            type="text"
                            name="destination"
                            onChange={(event) => onChangeHandler(event)}
                            defaultValue={inputValue.destination}
                            className={`${styles.to_terr} form-control`}
                            id="destination"
                            placeholder={t("common:city")}
                            required
                          />
                        </div>
                        <div className="col-12 col-md-4">
                          <div className="form-check mt-4">
                            <input
                              className="form-check-input"
                              type="checkbox"
                              onChange={onRoundTripChange}
                              checked={selectRoundTrip}
                              name="round_trip"
                              id="round_trip"
                            />
                            <label
                              className="form-check-label"
                              htmlFor="round_trip"
                            >
                              {t("common:roundTrip")}
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="mb-3">
                      <label htmlFor="loading_from" className="form-label">
                        {t("common:uploadDate")}
                        <span className="text-danger">*</span>
                      </label>
                      <div className="d-flex align-items-center">
                        <input
                          onChange={(event) => onChangeHandler(event)}
                          type="datetime-local"
                          min={getDate()}
                          name="loading_from"
                          defaultValue={inputValue.loading_from}
                          className="form-control"
                          id="loading_from"
                          required
                        />
                        <p className="ms-1 me-1 m-0">-</p>
                        <input
                          onChange={(event) => onChangeHandler(event)}
                          type="datetime-local"
                          min={getDate()}
                          name="loading_to"
                          defaultValue={inputValue.loading_to}
                          className="form-control"
                          required
                        />
                      </div>
                    </div>
                  </div>

                  <div
                    className={`${styles.addCar_container} shadow border rounded mt-3`}
                  >
                    <h3>{t("common:cargoDetails")}</h3>
                    <small className="form-text text-muted">
                      {t("common:cargoDetails")}
                    </small>
                    <div className="mb-3">
                      <label htmlFor="name" className="form-label">
                        {t("common:cargoName")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        type="text"
                        name="name"
                        onChange={(event) => onChangeHandler(event)}
                        defaultValue={inputValue.name}
                        className="form-control"
                        id="name"
                        placeholder={t("common:cargoName")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="packaging_type_id" className="form-label">
                        {t("common:typePackaging")}
                        <span className="text-danger">*</span>
                      </label>
                      <select
                        className="form-control"
                        name="packaging_type_id"
                        onChange={(event) => onChangeHandler(event)}
                        required
                      >
                        <option value="0">Փաթեթավորման տեսակը</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                      </select>
                    </div>
                    <div className="mb-3">
                      <label htmlFor="weight" className="form-label">
                        {t("common:weight")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        type="number"
                        min="0"
                        name="weight"
                        onChange={(event) => onChangeHandler(event)}
                        defaultValue={inputValue.weight}
                        className="form-control"
                        id="weight"
                        placeholder={t("common:tons")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="volume" className="form-label">
                        {t("common:volume")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        type="number"
                        min="0"
                        name="volume"
                        onChange={(event) => onChangeHandler(event)}
                        defaultValue={inputValue.volume}
                        className="form-control"
                        id="volume"
                        placeholder={t("common:cubeM")}
                        required
                      />
                    </div>

                    <div className="mt-3">
                      <select
                        className="form-control"
                        name="cargo_type"
                        onChange={(event) => onChangeHandler(event)}
                        required
                      >
                        {data &&
                          data.cargotype.map((e) => {
                            return (
                              <option key={e.id} value={e.id}>
                                {e["name_" + language]}
                              </option>
                            );
                          })}
                      </select>
                    </div>
                  </div>

                  <div
                    className={`${styles.addCar_container} shadow border rounded mt-3`}
                  >
                    <h3>{t("common:vehicleRequirements")}</h3>
                    <small className="form-text text-muted">
                      {t("common:vehicleRequirementsText")}
                    </small>
                    <div className="mb-3 mt-3">
                      <div className="row">
                        <div className="mb-3">
                          <label htmlFor="truck_type_id" className="form-label">
                            {t("common:needCar")}
                            <span className="text-danger">*</span>
                          </label>
                          <select
                            className="form-control"
                            name="truck_type_id"
                            id="truck_type_id"
                            onChange={(event) => onChangeHandler(event)}
                            required
                          >
                            <option value="0">
                              {t("common:chooseBody")}chooseBody
                            </option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="mb-3">
                      <label htmlFor="count_trucks" className="form-label">
                        {t("common:countCar")}{" "}
                        <span className="text-danger">*</span>
                      </label>
                      <input
                        type="number"
                        min="0"
                        name="count_trucks"
                        onChange={(event) => onChangeHandler(event)}
                        defaultValue={inputValue.count_trucks}
                        className="form-control"
                        id="count_trucks"
                        placeholder={t("common:cars")}
                        required
                      />
                    </div>
                    <div className="mb-3">
                      <div className="container">
                        <div className="row">
                          <label className="form-label">
                            {t("common:loading")}
                          </label>
                          <small className="form-text text-muted">
                            {t("common:loadingText")}
                          </small>
                          {data &&
                            data.loadingtype.map((e) => {
                              return (
                                <div key={e.id} className="col-12 col-md-4">
                                  <div className="form-check">
                                    <input
                                      onChange={loadingType}
                                      className="form-check-input"
                                      type="checkbox"
                                      name={e.id}
                                      id="one"
                                      defaultValue="Behind"
                                    />
                                    <label
                                      className="form-check-label"
                                      htmlFor="behind"
                                    >
                                      {e["name_" + language]}
                                    </label>
                                  </div>
                                </div>
                              );
                            })}
                        </div>
                      </div>
                    </div>
                    <div className="mb-3">
                      <div className="row">
                        <label className="form-label">{t("common:permissions")}</label>
                        <small className="form-text text-muted">
                        {t("common:permissionsText")}
                        </small>
                        <div className="col-6 col-md-3">
                          <div className="form-check">
                            <input
                              onChange={docPermission}
                              className="form-check-input"
                              type="checkbox"
                              name="tir"
                              id="tir"
                              defaultValue="Tir"
                            />
                            <label className="form-check-label" htmlFor="tir">
                              TIR
                            </label>
                          </div>
                        </div>
                        <div className="col-6 col-md-3">
                          <div className="form-check">
                            <input
                              onChange={docPermission}
                              className="form-check-input"
                              type="checkbox"
                              name="t1"
                              id="t1"
                              defaultValue="T1"
                            />
                            <label className="form-check-label" htmlFor="t1">
                              T1
                            </label>
                          </div>
                        </div>
                        <div className="col-6 col-md-3">
                          <div className="form-check">
                            <input
                              onChange={docPermission}
                              className="form-check-input"
                              type="checkbox"
                              name="cmr"
                              id="cmr"
                              defaultValue="Cmr"
                            />
                            <label className="form-check-label" htmlFor="cmr">
                              CMR
                            </label>
                          </div>
                        </div>
                        <div className="col-6 col-md-3">
                          <div className="form-check">
                            <input
                              onChange={docPermission}
                              className="form-check-input"
                              type="checkbox"
                              name="adr"
                              id="adr"
                              defaultValue="Adr"
                            />
                            <label className="form-check-label" htmlFor="adr">
                              ADR
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className={`${styles.addCar_container} shadow border rounded mt-3`}
                  >
                    <h3>{t("common:payment")}</h3>
                    <small className="form-text text-muted">
                    {t("common:paymentText")}
                    </small>

                    <div className="row">
                      <div className="col-8">
                        <label htmlFor="offer" className="form-label">
                        {t("common:offer")}
                        </label>
                        <input
                          type="number"
                          min="0"
                          name="offer"
                          className="form-control"
                          id="offer"
                          placeholder="0"
                          onChange={(event) => onChangeHandler(event)}
                          defaultValue={inputValue.offer}
                        />
                      </div>
                      <div className="col-4">
                        <label htmlFor="payment_type" className="form-label">
                        {t("common:paymentType")}
                        </label>
                        <select
                          className="form-control"
                          name="payment_type"
                          id="payment_type"
                          onChange={(event) => onChangeHandler(event)}
                          defaultValue={inputValue.payment_type}
                        >
                          <option value="0">{t("common:paymentType")}</option>
                          <option value="1">{t("common:cash")}</option>
                          <option value="2">{t("common:cashless")}</option>
                        </select>
                      </div>
                    </div>

                    <div className="col-12 text-end mt-5">
                      <button type="submit" className="btn btn-outline-primary">
                      {t("common:add")}
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default AddLoad;
