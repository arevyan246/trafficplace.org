import Head from "next/head";
import axios from "axios";
import { useState, useEffect } from "react";
import AboutUs from "../components/AboutUs";
import CallToAction from "../components/CallToAction";
import Faq from "../components/Faq";
import FeaturedServices from "../components/FeaturedServices";
import Features from "../components/Features";
import Slider from "../components/MainSlider";
import SuccessModal from "../components/Modal";
import OurServices from "../components/OurServices";
import Testimonials from "../components/Testimonials";
import apiUrl from "../configs/api-url";
import { useSelector } from "react-redux";
import useTranslation from "next-translate/useTranslation";

const Home = ({ data }) => {
  let { t } = useTranslation();
  const language = useSelector((state) => state.language.language);
  let message;

  if (typeof window !== "undefined") {
    message = localStorage.getItem("message");
    setTimeout(() => {
      localStorage.removeItem("message");
    }, 1000);
  }
  console.log(data, "data");
  return (
    <>
      {message && <SuccessModal massage={t('common:homeModal')} />}
      <Head>
        <title>{t('common:home')}</title>
      </Head>
      <Slider data={data?.message && data.message.slider} language={language} />
      {/* <FeaturedServices language={language} /> */}
      <AboutUs data={data?.message && data.message.about} language={language} />
      {/* <OurServices language={language} /> */}
      <CallToAction language={language} />
      <Features data={data?.message && data.message.advantages} language={language} />
      {/* <Testimonials language={language} /> */}
      <Faq data={data?.message && data.message.faq} language={language} />
    </>
  );
};

export async function getStaticProps() {
  const res = await fetch(apiUrl.home);
  const json = await res.json();
  return {
    props: {
      data: json,
    }
  };
}

export default Home;
