import Breadcrumbs from "../../components/Breadcrumbs";
import Router from "next/router";
import { useEffect } from "react";
import axios from "axios";
import { Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { getToken } from "../../features/user/userSlice";
import Link from "next/link";
import apiUrl from "../../configs/api-url";
import useTranslation from "next-translate/useTranslation";
import Head from "next/head";

const Registration = () => {
  let { t } = useTranslation();
  let accountId;
  const dispatch = useDispatch();
  let loading = useSelector((state) => state.user.loading);
  let errorMessage = useSelector((state) => state.user.errorMessage);

  if (typeof window !== "undefined") {
    accountId = localStorage.getItem("accountId");
  }

  useEffect(() => {
    if (accountId) {
      Router.push("/");
    }
  });

  const submitHandler = (inputValue) => {
    dispatch(getToken(inputValue));
    Router.push("/");
    if (typeof window !== "undefined") {
      localStorage.setItem("message", "succsess");
    }

    let values = {
      email: inputValue.email,
      password: inputValue.password,
    };
    axios
      .post(apiUrl.register, {
        email: inputValue.email,
        password: inputValue.password,
        is_accepted: inputValue.is_accepted,
      })
      .then((res) => dispatch(getToken(values)))
      .catch((e) => console.log(e.message));
  };

  return (
    <>
      <Head>
        <title>{t("common:registration")}</title>
      </Head>
      <Breadcrumbs title={t("common:registration")} />
      <div className="container-fluid p-5 m-0 form_bg">
        <Formik
          initialValues={{ email: "", password: "", is_accepted: false }}
          validate={(values) => {
            const errors = {};

            if (!values.email) {
              errors.email = t("common:emailRequired");
            } else if (
              !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
            ) {
              errors.email = t("common:emailIncomplete");
            }

            if (!values.is_accepted) {
              errors.is_accepted = t("common:isAccepted");
            }

            if (!values.password) {
              errors.password = t("common:emailPassword");
            } else if (values.password.length < 8) {
              errors.password = t("common:emailPasswordV8");
            }

            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            console.log(values, "values");
            submitHandler(values);
            setSubmitting(false);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <form
              onSubmit={handleSubmit}
              className="col-12 col-md-6 p-3 mx-auto border shadow rounded bg-light"
            >
              <div className="form-group">
                <small>
                  <label htmlFor="RegistrationEmail1">
                    {t("common:yourEmail")}
                  </label>
                </small>
                <input
                  onChange={handleChange}
                  defaultValue={values.email}
                  name="email"
                  type="email"
                  className="form-control"
                  id="RegistrationEmail1"
                  aria-describedby="emailHelp"
                  placeholder={t("common:yourEmail")}
                />
                <p className="text-danger">
                  {errors.email && touched.email && errors.email}
                </p>
              </div>

              <div className="form-group mt-2">
                <small>
                  <label htmlFor="RegistrationPassword1">
                    {t("common:yourPassword")}
                  </label>
                </small>
                <input
                  onChange={handleChange}
                  defaultValue={values.password}
                  name="password"
                  type="password"
                  className="form-control"
                  id="RegistrationPassword1"
                  placeholder={t("common:yourPassword")}
                />
                <p className="text-danger">
                  {errors.password && touched.password && errors.password}
                </p>
              </div>
              <div className="form-check mt-2">
                <input
                  // onChange={changeInputHandler}
                  onChange={handleChange}
                  type="checkbox"
                  name="is_accepted"
                  className="form-check-input"
                />
                <span className="form-check-label">
                  {t("common:isAcceptedText1")}
                  <span>
                    <Link href="/">
                      <a className="fw-bold text-dark" target="_blank">
                        {t("common:isAcceptedText2")}
                      </a>
                    </Link>
                  </span>
                  {t("common:isAcceptedText3")}{" "}
                  <span>
                    <Link href="/">
                      <a className="fw-bold text-dark" target="_blank">
                        {t("common:isAcceptedText4")}
                      </a>
                    </Link>
                  </span>
                  {t("common:isAcceptedText5")}
                </span>
                <p className="text-danger">
                  {errors.is_accepted &&
                    touched.is_accepted &&
                    errors.is_accepted}
                </p>
              </div>
              <div className="w-100 text-end">
                <button type="submit" className="btn btn-danger mt-4">
                  {t("common:register")}
                </button>
              </div>
            </form>
          )}
        </Formik>
      </div>
    </>
  );
};
export default Registration;
