import styles from "./About.module.css";
import Breadcrumbs from "../../components/Breadcrumbs";
import AboutUs from "../../components/AboutUs";
import Testimonials from "../../components/Testimonials";
import Faq from "../../components/Faq";
import useTranslation from "next-translate/useTranslation";
import { useSelector } from "react-redux";
import apiUrl from "../../configs/api-url";
import Head from "next/head";

const About = ({ data }) => {
  const language = useSelector((state) => state.language.language);
  let { t } = useTranslation();

  return (
    <>
      <Head>
        <title>{t("common:about")}</title>
      </Head>
      <Breadcrumbs title={t("common:about")} />
      <div className="container mt-3">
        <AboutUs
          data={data?.message && data.message.about}
          language={language}
        />
      </div>
      <div className="container-fluid ps-0 pe-0">
        <Testimonials language={language} />
      </div>
      <div className="container mb-3">
        <Faq data={data?.message && data.message.faq} language={language} />
      </div>
    </>
  );
};

export async function getStaticProps() {
  const res = await fetch(apiUrl.home);
  const json = await res.json();
  return {
    props: {
      data: json,
    },
  };
}

export default About;
