// import styles from "./Announcements.module.css";
import Image from "next/image";
import Link from "next/link";
import Breadcrumbs from "../../components/Breadcrumbs";
import useTranslation from "next-translate/useTranslation";
import { useSelector } from "react-redux";
import apiUrl from "../../configs/api-url";
import Head from "next/head";

const Announcements = ({ data }) => {
  let { t } = useTranslation();
  const language = useSelector((state) => state.language.language);
  const myLoader = ({ src, width, quality }) => {
    return `https://admin.trafficplace.org/storage/${src}?w=${width}&q=${
      quality || 75
    }`;
  };

  return (
    <>
    <Head>
      <title>{t("common:announcements")}</title>
    </Head>
      <Breadcrumbs title={t("common:announcements")} />
      <div className="container">
        <div className="list-group w-100 pb-3">
          {data &&
            data.message.map((item) => {
              return (
                <div
                  key={item.id}
                  className="row mt-3 border rounded shadow-sm"
                  aria-current="true"
                >
                  <div className="p-2 col-12 col-md-3 d-flex align-items-center">
                    <Image
                      src={item.image}
                      loader={myLoader}
                      width={1200}
                      height={531}
                      className="img-responsive"
                      alt={item["seo_title_" + language]}
                    />
                  </div>
                  <div className="p-2 col-12 col-md-9">
                    <div>
                      <h6 className="mb-0">{item["title_" + language]}</h6>
                      <p className="mb-0 opacity-75">
                        {item["description_" + language]}
                      </p>
                    </div>
                    <div className="text-end">
                      {item.youtube_link && (
                        <small className="text-nowrap me-2">
                          <Link href={item.youtube_link}>
                            <a target="_blank">
                              <i className="fa-brands fa-youtube text-danger"></i>
                              <span className="ms-1 fw-bold">YouTube</span>
                            </a>
                          </Link>
                        </small>
                      )}
                      {item.slug && (
                        <small className="text-nowrap">
                          <span className="me-1">|</span>
                          <Link
                            href={{
                              pathname: "/announcements/item",
                              query: {
                                result: data.result.className,
                                slug: item.slug,
                              },
                            }}
                          >
                            <a>
                              <span className="ms-1 fw-bold">
                                {t("common:learnMore")}
                              </span>
                              <i className="fa-solid fa-angles-right text-success"></i>
                            </a>
                          </Link>
                        </small>
                      )}
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </>
  );
};

export async function getStaticProps() {
  const res = await fetch(apiUrl.announcements);
  const json = await res.json();
  return {
    props: {
      data: json,
    },
  };
}

export default Announcements;
