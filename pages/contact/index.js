import styles from "./Contact.module.css";
import Breadcrumbs from "../../components/Breadcrumbs";
import useTranslation from "next-translate/useTranslation";
import axios from "axios";
import { useState } from "react";
import SuccessModal from "../../components/Modal";
import apiUrl from "../../configs/api-url";
import Router from "next/router";
import Head from "next/head";

const Contact = () => {
  let { t } = useTranslation();
  const [isSuccess, setIsSuccess] = useState(false);
  const [inputValue, setInputValue] = useState({
    name: "",
    email: "",
    message: "",
  });

  const onChangeHandler = (event) => {
    event.preventDefault();
    setInputValue({
      ...inputValue,
      [event.target.name]: event.target.value,
    });
  };

  const submitHandler = (event) => {
    event.preventDefault();
    axios
      .post(apiUrl.contact, {
        name: inputValue.name,
        email: inputValue.email,
        message: inputValue.message,
        subject: inputValue.message,
      })
      .then((res) => {
        setIsSuccess((pre) => !pre), Router.push('/contact');
      })
      .catch((e) => console.log(e.message));
  };

  return (
    <>
    <Head>
      <title>{t("common:feedback")}</title>
    </Head>
      {isSuccess && <SuccessModal massage={t("common:successContact")} />}
      <Breadcrumbs title={t("common:feedback")} />
      <div className="container mt-3 mb-3">
        <div className="row">
          <div>
            <iframe
              className={styles.google_map}
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d195167.57347126456!2d44.348480971491384!3d40.15356842368177!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x406aa2dab8fc8b5b%3A0x3d1479ae87da526a!2z0JXRgNC10LLQsNC9LCDQkNGA0LzQtdC90LjRjw!5e0!3m2!1sru!2s!4v1673389129685!5m2!1sru!2s"
              frameBorder="0"
              allowFullScreen
            ></iframe>
          </div>

          <div className="row gy-4 mt-4">
            <div className="col-lg-4">
              <div className={`${styles.info_item} d-flex`}>
                <i className="bi bi-geo-alt flex-shrink-0"></i>
                <div>
                  <h4>{t("common:location")}:</h4>
                  <p>Armenia Yerevan </p>
                </div>
              </div>

              <div className={`${styles.info_item} d-flex`}>
                <i className="bi bi-envelope flex-shrink-0"></i>
                <div>
                  <h4>{t("common:email")}:</h4>
                  <p>info@trafficplace.org</p>
                </div>
              </div>

              <div className={`${styles.info_item} d-flex`}>
                <i className="bi bi-phone flex-shrink-0"></i>
                <div>
                  <h4>{t("common:tel")}:</h4>
                  <p>+374 41448888</p>
                </div>
              </div>
            </div>

            <div className="col-lg-8">
              <form onSubmit={submitHandler} className={styles.php_email_form}>
                <div className="row">
                  <div className={`${styles.form_group} col-md-6`}>
                    <input
                      type="text"
                      name="name"
                      className="form-control"
                      id="name"
                      placeholder={t("common:name")}
                      onChange={(event) => onChangeHandler(event)}
                      required
                    />
                  </div>
                  <div className={`${styles.form_group} col-md-6 mt-3 mt-md-0`}>
                    <input
                      type="email"
                      className="form-control"
                      name="email"
                      id="email"
                      onChange={(event) => onChangeHandler(event)}
                      placeholder={t("common:email")}
                      required
                    />
                  </div>
                </div>
                <div className={`${styles.form_group} mt-3`}>
                  <textarea
                    className="form-control"
                    name="message"
                    rows="5"
                    minLength="6"
                    onChange={(event) => onChangeHandler(event)}
                    placeholder={t("common:message")}
                    required
                  ></textarea>
                </div>
                <div className="text-center">
                  <button type="submit">{t("common:sendMessage")}</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default Contact;
