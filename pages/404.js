import Link from "next/link";
import useTranslation from "next-translate/useTranslation";
import Head from "next/head";

const MyCustom404Page = () => {
  let { t } = useTranslation();
  return (
    <div className="_404_container">
    <Head>
      <title>404 {t("common:pageNotFound")}</title>
    </Head>
      <div className="_404_number">404</div>
      <div className="_404_text">
        <span>{t("common:pageNotFound")}</span>
        <br />
        <Link href="/">
          <a className="btn btn-success">
            <i className="fa-solid fa-house me-2"></i>
            {t("common:home")}
          </a>
        </Link>
        <br />
      </div>
    </div>
  );
};

export default MyCustom404Page;
