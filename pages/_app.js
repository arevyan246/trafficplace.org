import Layout from "../components/Layout";
import "../styles/globals.css";
import { Provider } from "react-redux";
import { store } from "../store/store";
import { Html, Head } from "next/document";
import ErrorBoundary from "../components/ErrorBoundary";
const MyApp = ({ Component, pageProps }) => {
  return (
    <Provider store={store}>
      <Layout>
        <ErrorBoundary>
          <Component {...pageProps} />
        </ErrorBoundary>
      </Layout>
    </Provider>
  );
};

export default MyApp;
