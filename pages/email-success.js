import Link from "next/link";
import useTranslation from "next-translate/useTranslation";
import Head from "next/head";

const EmailSuccess = () => {
  let { t } = useTranslation();
  return (
    <div className="_404_container">
      <Head>
        <title>Email Success</title>
      </Head>
      <div className="_404_text mt-5 mb-5 pt-5 pb-5">
        <span className="text-success">{t("common:emailVerifiedSuccess")}</span>
        <br />
        <Link href="/">
          <a className="btn btn-success mt-5 mb-5">
            <i className="fa-solid fa-house me-2"></i>
            {t("common:home")}
          </a>
        </Link>
        <br />
      </div>
    </div>
  );
};

export default EmailSuccess;
