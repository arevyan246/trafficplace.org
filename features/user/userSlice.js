import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import config from "../../configs/api-url";
import useStorage from "../../hooks/useStorage";
const initialState = {
  token: "",
  loading: false,
  errorMessage: "",
  expireTokenTime: null,
  accountId: 0,
  language: "am",
};

export const getToken = createAsyncThunk(
  "user/getToken",
  async (values, { rejectWithValue, dispatch }) => {
    let response;
    dispatch(setLoading(true));
    dispatch(setErrorMessage(""));
    await axios
      .post(config.login, values)
      .then((res) => {
        response = res;
        if (response.data.error) {
          dispatch(setErrorMessage(response.data.error));
        }
        dispatch(setLoading(false));
      })
      .catch((err) => {
        console.log(err, "err");
        dispatch(setLoading(false));
      });

    dispatch(setToken(response.data.access_token));
    dispatch(setAccountId(response.data.message.account_id));
    useStorage().setItem("token", response.data.access_token);
    useStorage().setItem("accountId", response.data.message.account_id);
  }
);

export const logOut = createAsyncThunk(
  "user/logOut",
  async (values, { rejectWithValue, dispatch }) => {
    dispatch(setLoading(true));
    dispatch(setErrorMessage(""));
    localStorage.clear();
    dispatch(setToken(""));
    try {
      const response = await axios.post(config.logout);
      // navigate('/',{state: {token : "response?.data?.token"}});
      dispatch(setLoading(false));
      console.log(response, "logout response");
      if (response.data.status) {
        useStorage().setItem("token", "");
        dispatch(setToken(""));
      }
    } catch (err) {
      console.log(err, "err");
      dispatch(setLoading(false));
      if (err?.response?.data?.message) {
        dispatch(setErrorMessage(err.response.data.message));
      } else {
        dispatch(setErrorMessage("Something went wrong"));
      }
    }
  }
);

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setToken: (state, action) => {
      state.token = action.payload;
    },
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
    setErrorMessage: (state, action) => {
      state.errorMessage = action.payload;
    },
    setExpireTokenTime: (state, action) => {
      state.expireTokenTime = action.payload;W
    },
    setAccountId: (state, action) => {
      state.accountId = action.payload;
    },
  },
  extraReducers: {
    [getToken.fulfilled]: () => {
      // console.log("getToken fullfiled")
    },
    [getToken.pending]: () => {
      // console.log("getToken pending")
    },
    [getToken.rejected]: () => {
      // console.log("getToken rejected")
    },
  },
});



export const {
  setToken,
  setLoading,
  setErrorMessage,
  setExpireTokenTime,
  setAccountId,
} = userSlice.actions;
export default userSlice.reducer;
