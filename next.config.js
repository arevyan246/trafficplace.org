/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  swcMinify: true,
};
const nextTranslate = require("next-translate");
module.exports = {
  ...nextTranslate(),
  nextConfig,
};
