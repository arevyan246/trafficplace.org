import Link from "next/link";
import styles from "./Breadcrumbs.module.css";
import useTranslation from "next-translate/useTranslation";

const Breadcrumbs = ({ title, text = null }) => {
  let { t } = useTranslation();
  return (
    <div className={styles.breadcrumbs}>
      <div className={`${styles.page_header} d-flex align-items-center`}>
        <div className="container position-relative">
          <div className="row d-flex justify-content-center">
            <div className="col-lg-6 text-center">
              <h2>{title}</h2>
              {text && <p>
                {text}
              </p>}
            </div>
          </div>
        </div>
      </div>
      <nav>
        <div className="container">
          <ol>
            <li>
              <Link href="/">
                <a>{t('common:home')}</a>
              </Link>

            </li>
            <li>{title}</li>
          </ol>
        </div>
      </nav>
    </div>
  );
};

export default Breadcrumbs;
