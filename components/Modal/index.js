import React from "react";

import { Modal, ModalBody } from "reactstrap";

const SuccessModal = ({massage}) => {
  const [modalOpen, setModalOpen] = React.useState(true);
  return (
    <Modal toggle={() => setModalOpen(!modalOpen)} isOpen={modalOpen}>
      <div className="border-bottom-0 modal-header">
        <button
          aria-label="Close"
          className="btn btn-danger ms-auto close"
          type="button"
          onClick={() => setModalOpen(!modalOpen)}
        >
          <span aria-hidden={true}>X</span>
        </button>
      </div>
      <ModalBody>
          <div className="container text-center">
            <i className="fa-solid fa-circle-check text-success fa-beat-fade fa-6x"></i>
            <h3 className="mt-3">{massage}</h3>
        </div>
      </ModalBody>
    </Modal>
  );
};

export default SuccessModal;
