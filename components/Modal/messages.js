import React, { useState } from "react";

import { Modal, ModalBody } from "reactstrap";
import useTranslation from "next-translate/useTranslation";
import axios from "axios";
import apiUrl from "../../configs/api-url";
import { useSelector } from "react-redux";

const MessagesModal = ({ open, closeModalMessage, userInfo, selfUser }) => {
  let { t } = useTranslation();
  const token = useSelector((state) => state.user.token);
  const [inputValue, setInputValue] = useState({
    message: "",
    name: "",
    email: "",
  });

  const onChangeHandler = (event) => {
    event.preventDefault();
    setInputValue({
      ...inputValue,
      [event.target.name]: event.target.value,
    });
  };

  const submitHandler = (event) => {
    event.preventDefault();
    axios
      .post(
        apiUrl.message,
        {
          message: inputValue.message,
          user_id: selfUser,
        },
        {
          headers: {
            Authorization: "bearer " + token,
          },
        }
      )
      .then((res) => {
        Router.push("/cars");
      })
      .catch((e) => console.log(e.message));
  };

  return (
    <Modal toggle={() => closeModalMessage()} isOpen={open}>
      <div className="border-bottom-0 modal-header">
      <h4>{t("common:sendMessage")}</h4>
        <button
          aria-label="Close"
          className="btn btn-danger ms-auto close"
          type="button"
          onClick={() => closeModalMessage()}
        >
          <span aria-hidden={true}>X</span>
        </button>
      </div>
      <ModalBody>
        <div className="container">
          <form onSubmit={submitHandler}>
            <div className="mb-3">
              <label htmlFor="message" className="form-label">
                {t("common:message")} <span className="text-danger">*</span>
              </label>
              <textarea
                name="message"
                type="text"
                id="message"
                className="form-control"
                onChange={(event) => onChangeHandler(event)}
                defaultValue={inputValue.message}
                rows='5'
                required
              ></textarea>
            </div>
            <div className="mb-3">
              <label htmlFor="name" className="form-label">
                {t("common:name")} <span className="text-danger">*</span>
              </label>
              <input
                name="name"
                type="text"
                id="name"
                className="form-control"
                defaultValue={userInfo.name}
                readOnly
                required
              />
            </div>
            <div className="mb-3">
              <label htmlFor="email" className="form-label">
                {t("common:email")} <span className="text-danger">*</span>
              </label>
              <input
                name="email"
                type="email"
                id="email"
                className="form-control"
                defaultValue={userInfo.email}
                readOnly
                required
              />
            </div>
            <div className="col-12 mt-5 text-end">
              <button type="submit" className="btn btn-outline-success">
                {t("common:sendMessage")}
              </button>
            </div>
          </form>
        </div>
      </ModalBody>
    </Modal>
  );
};

export default MessagesModal;
