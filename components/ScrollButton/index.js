import Link from "next/link";
import { useState, useEffect } from "react";
import styles from "./ScrollButton.module.css";

const ScrollButton = ({ text, urlLink }) => {
  const [isVisible, setIsVisible] = useState(true);
  const [height, setHeight] = useState(0);

  useEffect(() => {
    window.addEventListener("scroll", listenToScroll);
    return () => window.removeEventListener("scroll", listenToScroll);
  }, []);

  const listenToScroll = () => {
    let heightToHideFrom = 1000;
    const winScroll =
      document.body.scrollTop || document.documentElement.scrollTop;
    setHeight(winScroll);

    if (winScroll > heightToHideFrom) {
      isVisible && setIsVisible(false);
    } else {
      setIsVisible(true);
    }
  };

  return (
    <>
      {isVisible && (
        <div id={styles.scroll_btn}>
          <Link href={urlLink}>
            <p className="btn shadow" id={styles.scroll_btn_text}>{text}</p>
          </Link>
        </div>
      )}
    </>
  );
};

export default ScrollButton;
