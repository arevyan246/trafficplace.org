import { useEffect, useState } from "react";
import useTranslation from "next-translate/useTranslation";
import apiUrl from "../../configs/api-url";
import axios from "axios";

const SearchForm = ({filterResultHandler}) => {
  let { t } = useTranslation();
  const [show, setShow] = useState(false);
  let buttonText = show
    ? `${t("common:simpleSearch")}`
    : `${t("common:advancedSearch")}`;
  const toggleShow = () => setShow(!show);
  const [filterInputs, setFilterInputs] = useState(null);
  const [resultFilter,setResultFilter] = useState(null);
  
  const changeInputs = (e) => {
    setFilterInputs({
      ...filterInputs,
      type:"Truck",
      [e.target.name]: e.target.value,
    });
  };
  const submitFilter = ()=>{
    axios.post(apiUrl.filter,filterInputs).then((res)=>{
      if(res.data.message){
        setResultFilter(res.data.message)
      }
    }).catch((err)=>{
      console.log(err ,"errrrr")
    })
  }
  useEffect(()=>{
    filterResultHandler(resultFilter)
  },[resultFilter])

  // console.log(filterInputs, "cincincicni");
  return (
    <div className="col-12 border rounded p-3">
      <form>
        <div className="row">
          <div className="col-12 col-md-2">
            <label htmlFor="date_from">{t("common:dates")}</label>
            <input
              onChange={(e) => changeInputs(e)}
              type="date"
              name="date_from"
              className="form-control"
              id="date_from"
            />
          </div>
          <div className="col-12 col-md-2">
            <label htmlFor="date_to">{t("common:dates")}</label>
            <input
              onChange={(e) => changeInputs(e)}
              type="date"
              name="date_to"
              className="form-control"
              id="date_to"
            />
          </div>
          <div className="col-12 col-md-8">
            <div className="row">
              <div className="col-12 col-md-6">
                <label htmlFor="from">{t("common:startRoute")}</label>
                <input
                  onChange={(e) => changeInputs(e)}
                  type="text"
                  name="from"
                  className="form-control"
                  id="from"
                  placeholder={t("common:where")}
                />
              </div>
              <div className="col-12 col-md-6">
                <label htmlFor="destination">{t("common:destination")}</label>
                <input
                  onChange={(e) => changeInputs(e)}
                  type="text"
                  name="destination"
                  className="form-control"
                  id="destination"
                  placeholder={t("common:where")}
                />
              </div>
            </div>
          </div>
        </div>
        {/* <div className={`${show ? "" : "d-none"} row mt-2 d-flex`}>
          <div className="mb-1 mt-1 col-12 col-md-3 d-flex align-items-center">
            <div className="form-group w-100">
              <select className="form-control">
                <option>{t("common:chooseBody")}</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </select>
            </div>
          </div>
          <div className="mb-1 mt-1 col-12 col-md-3 d-flex align-items-center">
            <div className="form-group w-100">
              <select className="form-control">
                <option>{t("common:anyDownload")}</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </select>
            </div>
          </div>
          <div className="mb-1 mt-1 col-12 col-md-6 d-flex align-items-center">
            <div className="form-control d-flex justify-content-center">
              <input
                className="w-100 border-0"
                type="number"
                min="0"
                name=""
                placeholder={t("common:from")}
              />
              
              <input
                className="w-100 border-0"
                type="number"
                min="0"
                name=""
                placeholder={t("common:before")}
              />
            </div>
            <span> {t("common:tons")}</span>
          </div>
          <div className="mb-1 mt-1 col-12 col-md-6 d-flex align-items-center">
            <div className="form-control d-flex justify-content-center">
              <input
                className="w-100 border-0"
                type="number"
                min="0"
                name=""
                placeholder={t("common:from")}
              />
              
              <input
                className="w-100 border-0"
                type="number"
                min="0"
                name=""
                placeholder={t("common:before")}
              />
            </div>
            <span>{t("common:cubeM")}</span>
          </div>
        </div> */}
        <div className="row">
          {/* <div className="col-6 text-start mt-2">
            <button
              type="button"
              className="btn btn-light"
              onClick={() => toggleShow()}
            >
              {buttonText}
            </button>
          </div> */}
          <div className="col-12 text-end mt-2 ">
            <button type="button" className={`btn btn-primary ${!filterInputs && "disabled" }`} onClick={submitFilter}>{t("common:find")}</button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default SearchForm;
