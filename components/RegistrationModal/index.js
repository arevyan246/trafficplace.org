import React, { useState } from "react";
import axios from "axios";
import { Formik } from "formik";
import Link from "next/link";
import { useDispatch } from "react-redux";
import { getToken } from "../../features/user/userSlice";
import Router from "next/router";
import apiUrl from "../../configs/api-url";
import useTranslation from "next-translate/useTranslation";

const RegistrationModal = () => {
  let { t } = useTranslation();
  const dispatch = useDispatch();
  const submitHandler = (inputValue) => {
    if (typeof window !== "undefined") {
      localStorage.setItem("message", "succsess");
    }
    let values = {
      email: inputValue.email,
      password: inputValue.password,
    };
    axios
      .post(apiUrl.register, {
        email: inputValue.email,
        password: inputValue.password,
        is_accepted: inputValue.is_accepted,
      })
      .then((res) => (dispatch(getToken(values)), Router.push("/")))
      .catch((e) => console.log(e.message));
  };

  return (
    <div
      className="modal fade"
      id="registrationModal"
      tabIndex="-1"
      aria-labelledby="registrationModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="registrationModalLabel">
              {t("common:registration")}
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="modal-body p-4">
            <Formik
              initialValues={{ email: "", password: "", is_accepted: false }}
              validate={(values) => {
                const errors = {};

                if (!values.email) {
                  errors.email = t("common:emailRequired");
                } else if (
                  !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                ) {
                  errors.email = t("common:emailIncomplete");
                }

                if (!values.is_accepted) {
                  errors.is_accepted = t("common:isAccepted");
                }

                if (!values.password) {
                  errors.password = t("common:emailPassword");
                } else if (values.password.length < 8) {
                  errors.password = t("common:emailPasswordV8");
                }

                return errors;
              }}
              onSubmit={(values, { setSubmitting }) => {
                console.log(values, "values");
                submitHandler(values);
                setSubmitting(false);
              }}
            >
              {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
              }) => (
                <form onSubmit={handleSubmit}>
                  <div className="form-group">
                    <small>
                      <label htmlFor="RegistrationEmail">
                        {t("common:yourEmail")}
                      </label>
                    </small>
                    <input
                      onChange={handleChange}
                      defaultValue={values.email}
                      name="email"
                      type="email"
                      className="form-control"
                      id="RegistrationEmail"
                      aria-describedby="emailHelp"
                      placeholder={t("common:yourEmail")}
                    />
                    <p className="text-danger">
                      {errors.email && touched.email && errors.email}
                    </p>
                  </div>

                  <div className="form-group mt-2">
                    <small>
                      <label htmlFor="RegistrationPassword">
                        {t("common:yourPassword")}
                      </label>
                    </small>
                    <input
                      onChange={handleChange}
                      defaultValue={values.password}
                      name="password"
                      type="password"
                      className="form-control"
                      id="RegistrationPassword"
                      placeholder={t("common:yourPassword")}
                    />
                    <p className="text-danger">
                      {errors.password && touched.password && errors.password}
                    </p>
                  </div>
                  <div className="form-check mt-2">
                    <input
                      // onChange={changeInputHandler}
                      onChange={handleChange}
                      type="checkbox"
                      name="is_accepted"
                      className="form-check-input"
                    />
                    <span className="form-check-label">
                      {t("common:isAcceptedText1")}
                      <span>
                        <Link href="/">
                          <a className="fw-bold text-dark" target="_blank">
                            {t("common:isAcceptedText2")}
                          </a>
                        </Link>
                      </span>
                      {t("common:isAcceptedText3")}
                      <span>
                        <Link href="/">
                          <a className="fw-bold text-dark" target="_blank">
                            {t("common:isAcceptedText4")}
                          </a>
                        </Link>
                      </span>
                      {t("common:isAcceptedText5")}
                    </span>
                    <p className="text-danger">
                      {errors.is_accepted &&
                        touched.is_accepted &&
                        errors.is_accepted}
                    </p>
                  </div>
                  <button
                    type="submit"
                    className="btn btn-danger w-100 mt-4"
                    data-bs-dismiss="modal"
                  >
                    {t("common:register")}
                  </button>
                </form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RegistrationModal;
