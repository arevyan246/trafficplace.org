import Link from 'next/link';
import styles from './FeaturedServices.module.css';
import useTranslation from "next-translate/useTranslation";

const FeaturedServices = () => {
  let { t } = useTranslation();
  return (
    <section id="featured-services" className={styles.featured_services}>
      <div className="container">
        <div className="row gy-4">
          <div
            className={`${styles.service_item} col-lg-4 col-md-6 d-flex`}
            data-aos="fade-up"
          >
            <div className={`${styles.icon} flex-shrink-0`}>
              <i className="fa-solid fa-cart-flatbed"></i>
            </div>
            <div>
              <h4 className={styles.title}>Lorem Ipsum</h4>
              <p className={styles.description}>
                Voluptatum deleniti atque corrupti quos dolores et quas
                molestias excepturi sint occaecati cupiditate non provident
              </p>
              <Link href="service-details.html">
                <a className={`${styles.readmore} stretched-link`}>
                  <span>{t('common:learnMore')}</span>
                  <i className="fa-sharp fa-solid fa-arrow-right"></i>
                </a>
              </Link>
            </div>
          </div>

          <div
            className={`${styles.service_item} col-lg-4 col-md-6 d-flex`}
            data-aos="fade-up"
            data-aos-delay="100"
          >
            <div className={`${styles.icon} flex-shrink-0`}>
              <i className="fa-solid fa-truck"></i>
            </div>
            <div>
              <h4 className={styles.title}>Dolor Sitema</h4>
              <p className={styles.description}>
                Minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                aliquip ex ea commodo consequat tarad limino ata
              </p>
              <Link href="service-details.html" >
                <a className={`${styles.readmore} stretched-link`}>
                  <span>{t('common:learnMore')}</span>
                  <i className="fa-sharp fa-solid fa-arrow-right"></i>
                </a>
              </Link>
            </div>
          </div>

          <div
            className={`${styles.service_item} col-lg-4 col-md-6 d-flex`}
            data-aos="fade-up"
            data-aos-delay="200"
          >
            <div className={`${styles.icon} flex-shrink-0`}>
              <i className="fa-solid fa-truck-ramp-box"></i>
            </div>
            <div>
              <h4 className={styles.title}>Sed ut perspiciatis</h4>
              <p className={styles.description}>
                Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur
              </p>
              <Link href="service-details.html">
              <a className={`${styles.readmore} stretched-link`}>
                <span>{t('common:learnMore')}</span>
                <i className="fa-sharp fa-solid fa-arrow-right"></i>
              </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default FeaturedServices;
