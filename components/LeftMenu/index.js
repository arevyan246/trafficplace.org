import Link from "next/link";
import styles from "./LeftMenu.module.css";
import useTranslation from "next-translate/useTranslation";

const LeftMenu = () => {
  let { t } = useTranslation();
  return (
    <div className={`${styles.leftMenu} col-2 px-0`}>
      <div className="d-flex flex-column align-items-center align-items-sm-start px-1 pt-3 text-white min-vh-100">
        <div
          className="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start list-group w-100"
          id="menu"
        >
          <Link href="/cabinet">
            <a className="justify-content-center justify-content-sm-start p-1 mb-2 rounded list-group-item-primary list-group-item-action align-items-center d-flex">
              <i className="m-1 fs-4 fa-solid fa-computer"></i>
              <span className=" ms-1 d-none d-sm-inline text-truncate">{t("common:myCabinet")}</span>
            </a>
          </Link>
          <div className="dropdown w-100">
            {/* <Link href="/b">
              <a className="dropdown-toggle justify-content-center justify-content-sm-start p-1 mb-2 rounded list-group-item-primary list-group-item-action align-items-center d-flex" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <i className="m-1 fs-4 fa-regular fa-message"></i>
                <span className=" ms-1 d-none d-sm-inline text-truncate">{t("common:messages")}</span>
              </a>
            </Link> */}
            <ul className="dropdown-menu">
              <li>
                <Link href="/cabinet/message/inbox">
                  <a className="dropdown-item">{t("common:inbox")}</a>
                </Link>
              </li>
            </ul>
          </div>
          <div className="dropdown w-100">
            <Link href="/b">
              <a className="dropdown-toggle justify-content-center justify-content-sm-start p-1 mb-2 rounded list-group-item-primary list-group-item-action align-items-center d-flex" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <i className="m-1 fs-4 fa-regular fa-plus"></i>
                <span className=" ms-1 d-none d-sm-inline text-truncate">{t("common:add")}</span>
              </a>
            </Link>
            <ul className="dropdown-menu">
              <li>
                <Link href="/cabinet/add-load">
                  <a className="dropdown-item">{t("common:load")}</a>
                </Link>
              </li>
              <li>
                <Link href="/cabinet/add-car">
                  <a className="dropdown-item">{t("common:car")}</a>
                </Link>
              </li>
            </ul>
          </div>
          <div className="dropdown w-100">
            <Link href="/dhg">
              <a className="dropdown-toggle justify-content-center justify-content-sm-start p-1 mb-2 rounded list-group-item-primary list-group-item-action align-items-center d-flex" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <i className="m-1 fs-4 fa-regular fa-user"></i>
                <span className=" ms-1 d-none d-sm-inline text-truncate">{t("common:editProfile")}</span>
              </a>
            </Link>
            <ul className="dropdown-menu">
              <li>
                <Link href="/cabinet/profile/edit">
                  <a className="dropdown-item">{t("common:editProfile")}</a>
                </Link>
              </li>
              <li>
                <Link href="/cabinet/profile/edit-password">
                  <a className="dropdown-item">{t("common:changePassword")}</a>
                </Link>
              </li>
              {/* <li>
                <Link href="/cabinet/profile/edit-email">
                  <a className="dropdown-item">{t("common:changeEmail")}</a>
                </Link>
              </li> */}
              <hr className="my-1" />
              <li>
                <Link href="/delete">
                  <a className="dropdown-item text-danger">{t("common:deleteProfile")}</a>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LeftMenu;
