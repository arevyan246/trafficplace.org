import Image from 'next/image';
import Link from 'next/link';
import Script from 'next/script';
import styles from './Footer.module.css';
import useTranslation from "next-translate/useTranslation";

const Footer = () => {
    let { t } = useTranslation();
    return (
        <>
            <footer className={`${styles.footer_bg}`}>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-lg-5 col-md-12 footer-info text-center text-md-start">
                            <Link href="/">
                                <a className="navbar-brand text-white d-flex align-items-center mb-3">
                                    <Image className='img-fluid' src={'/logo.png'} width={150} height={120} />
                                </a>
                            </Link>
                            <h4 className="pb-0 text-white">{t("common:about")}</h4>
                            <div className="d-flex">
                                <p className={styles.footer_text}>{t("common:aboutustextfooter")}
                                </p>
                            </div>
                        </div>

                        <div className="col-lg-3 col-md-12 footer-contact text-center text-md-start">
                            <h4 className='text-white'>{t("common:feedback")}</h4>
                            <p className={styles.footer_text}>
                                Armenia<br />
                                Yerevan<br />
                                Abovyan st. <br /><br />
                                <strong>{t("common:tel")}:</strong> +374 41448888<br />
                                <strong>{t("common:email")}:</strong> info@trafficplace.org<br />
                            </p>

                        </div>

                        <div className="col-lg-3 col-md-12 footer-contact text-center text-md-start">
                            <h4 className='text-white'>{t("common:socialNetworks")}</h4>
                            <div className="social-links text-center text-md-start">
                                <Link href="/1">
                                    <a className={`${styles.footer_link} m-1`}>
                                        <i className="fa-brands fa-twitter fa-xl"></i>
                                    </a>
                                </Link>
                                <Link href="/2">
                                    <a className={`${styles.footer_link} m-1`}>
                                        <i className="fa-brands fa-facebook fa-xl"></i>
                                    </a>
                                </Link>
                                <Link href="/3">
                                    <a className={`${styles.footer_link} m-1`}>
                                        <i className="fa-brands fa-instagram fa-xl"></i></a>
                                </Link>
                                <Link href="/4">
                                    <a className={`${styles.footer_link} m-1`}>
                                        <i className="fa-brands fa-linkedin fa-xl"></i>
                                    </a>
                                </Link>
                            </div>
                            <h4 className="mt-4 pb-0 text-white">{t("common:download")}</h4>
                            <div className="row">
                                <div className="col-6">
                                    <Link href="https://www.apple.com/ru/app-store/">
                                        <a target="_blank">
                                            <Image className="img-fluid" src="/apple.png" alt="Apple" width={128} height={38} />
                                        </a>
                                    </Link>
                                </div>
                                <div className="col-6">
                                    <Link href="https://play.google.com/store/games">
                                        <a target="_blank">
                                            <Image className="img-fluid" src="/google.png" alt="Google" width={128} height={38} />
                                        </a>
                                    </Link>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className="text-center text-white pt-4 border-top">
                        <p>{t('common:copyrightstart')} © {new Date().getFullYear()} {t('common:copyright')}</p>
                    </div>
                </div>
            </footer>

            <Script  src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></Script>
            <Script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAw2ttThDNYHU_XPe5t3HQgQS97f5dOxck&libraries=places"></Script>
            <Script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" crossOrigin="anonymous"></Script>
            <Script src="https://cdnjs.cloudflare.com/ajax/libs/glightbox/3.2.0/js/glightbox.min.js" crossOrigin="anonymous" referrerPolicy="no-referrer"></Script>
            <Script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/8.3.2/swiper-bundle.min.js" crossOrigin="anonymous" referrerPolicy="no-referrer"></Script>
            {/* <Script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></Script> */}
        </>
    )
}

export default Footer;
