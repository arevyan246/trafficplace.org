import Link from "next/link";
import styles from "./AboutTheSystem.module.css";
import useTranslation from "next-translate/useTranslation";

const AboutTheSystem = () => {
  let { t } = useTranslation();

  return (
    <div className="col-12 mt-3">
      <div className={styles.under_list_info}>
        <div>
          <p className={styles.title}>{t("common:theSystem")}</p>
          <p>{t("common:theSystemText")}</p>
          <p className={styles.title}>{t("common:loads")}</p>
          <p>{t("common:loadsText1")}</p>
          <p>{t("common:loadsText2")}</p>
          <p>{t("common:loadsText3")}</p>
          <p>{t("common:loadsText4")}</p>
          <p className={styles.title}>{t("common:cargoSearch")}</p>
          <p>{t("common:cargoSearchText1")}</p>
          <p>{t("common:cargoSearchText2")}</p>
          <p>{t("common:cargoSearchText3")}</p>
          <p className={styles.title}>{t("common:newsletter")}</p>
          <p>{t("common:newsletterText1")}</p>
          <p>{t("common:newsletterText2")}</p>
          <p>{t("common:newsletterText3")}</p>
          <p className={styles.title}>{t("common:archive")}</p>
          <p>{t("common:archiveText1")}</p>
          <p>{t("common:archiveText2")}</p>
          <p>{t("common:archiveText3")}</p>
          <p className={styles.title}>{t("common:notebook")}</p>
          <p>{t("common:notebookText1")}</p>
          <p>{t("common:notebookText2")}</p>
          <p>{t("common:notebookText3")}</p>
          <p className={styles.title}>{t("common:selectCargoMap")}</p>
          <p>{t("common:selectCargoMapText1")}</p>
          <p>{t("common:selectCargoMapText2")}</p>
        </div>
      </div>
    </div>
  );
};

export default AboutTheSystem;
