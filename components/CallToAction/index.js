import Link from "next/link";
import styles from "./CallToAction.module.css";
import useTranslation from "next-translate/useTranslation";

const CallToAction = () => {
  let { t } = useTranslation();
  return (
    <section id="call-to-action" className={styles.call_to_action}>
      <div className="container" data-aos="zoom-out">

        <div className="row justify-content-center">
          <div className="col-lg-8 text-center">
            <h3>{t('common:callToAction')}</h3>
            <p>{t('common:callToActionText')}</p>
            <Link href="/cl">
              <a className={styles.cta_btn}>{t('common:callToAction')}</a>
            </Link>
          </div>
        </div>

      </div>
    </section>
  );
};

export default CallToAction;
