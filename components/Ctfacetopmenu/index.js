import styles from "./CtfacetopMenu.module.css";

const CtfacetopMenu = ({name, count}) => {
  return (
    <div className="col-12 col-md-4">
      <p className={`${styles.all_cargo} text-dark`}>
        {name}
        <span className={styles.all_cargo_count}>{count}</span>
      </p>
    </div>
  );
};

export default CtfacetopMenu;
