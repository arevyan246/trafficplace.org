import Image from "next/image";
import Link from "next/link";
import styles from "./OurServices.module.css";
import useTranslation from "next-translate/useTranslation";

const OurServices = ({ language }) => {
  let { t } = useTranslation();
  return (
    <section id="service" className={`${styles.services} pt-0`}>
      <div className="container" data-aos="fade-up">
        <div className="section-header">
          <span>{t('common:ourServices')}</span>
          <h2>{t('common:ourServices')}</h2>
        </div>

        <div className="row gy-4">
          <div
            className="col-lg-4 col-md-6"
            data-aos="fade-up"
            data-aos-delay="100"
          >
            <div className={styles.card}>
              <div className={styles.card_img}>
                <Image
                  width={800}
                  height={600}
                  src="/services/storage-service.jpg"
                  alt=""
                  className="img-fluid"
                />
              </div>
              <h3>
                <Link href="service-details.html">
                  <a className="stretched-link">Storage</a>
                </Link>
              </h3>
              <p>
                Cumque eos in qui numquam. Aut aspernatur perferendis sed atque
                quia voluptas quisquam repellendus temporibus itaqueofficiis
                odit
              </p>
            </div>
          </div>

          <div
            className="col-lg-4 col-md-6"
            data-aos="fade-up"
            data-aos-delay="200"
          >
            <div className={styles.card}>
              <div className={styles.card_img}>
                <Image
                  width={800}
                  height={600}
                  src="/services/logistics-service.jpg"
                  alt=""
                  className="img-fluid"
                />
              </div>
              <h3>
                <Link href="service-details.html">
                  <a className="stretched-link">Logistics</a>
                </Link>
              </h3>
              <p>
                Asperiores provident dolor accusamus pariatur dolore nam id
                audantium ut et iure incidunt molestiae dolor ipsam ducimus
                occaecati nisi
              </p>
            </div>
          </div>

          <div
            className="col-lg-4 col-md-6"
            data-aos="fade-up"
            data-aos-delay="300"
          >
            <div className={styles.card}>
              <div className={styles.card_img}>
                <Image
                  width={800}
                  height={600}
                  src="/services/cargo-service.jpg"
                  alt=""
                  className="img-fluid"
                />
              </div>
              <h3>
                <Link href="service-details.html">
                  <a className="stretched-link">Cargo</a>
                </Link>
              </h3>
              <p>
                Dicta quam similique quia architecto eos nisi aut ratione aut
                ipsum reiciendis sit doloremque oluptatem aut et molestiae ut et
                nihil
              </p>
            </div>
          </div>

          <div
            className="col-lg-4 col-md-6"
            data-aos="fade-up"
            data-aos-delay="400"
          >
            <div className={styles.card}>
              <div className={styles.card_img}>
                <Image
                  width={800}
                  height={600}
                  src="/services/trucking-service.jpg"
                  alt=""
                  className="img-fluid"
                />
              </div>
              <h3>
                <Link href="service-details.html">
                  <a className="stretched-link">Trucking</a>
                </Link>
              </h3>
              <p>
                Dicta quam similique quia architecto eos nisi aut ratione aut
                ipsum reiciendis sit doloremque oluptatem aut et molestiae ut et
                nihil
              </p>
            </div>
          </div>

          <div
            className="col-lg-4 col-md-6"
            data-aos="fade-up"
            data-aos-delay="500"
          >
            <div className={styles.card}>
              <div className={styles.card_img}>
                <Image
                  width={800}
                  height={600}
                  src="/services/packaging-service.jpg"
                  alt=""
                  className="img-fluid"
                />
              </div>
              <h3>
                <Link href="service-details.html">
                  <a className="stretched-link">Packaging</a>
                </Link>
              </h3>
              <p>
                Illo consequuntur quisquam delectus praesentium modi dignissimos
                facere vel cum onsequuntur maiores beatae consequatur magni
                voluptates
              </p>
            </div>
          </div>

          <div
            className="col-lg-4 col-md-6"
            data-aos="fade-up"
            data-aos-delay="600"
          >
            <div className={styles.card}>
              <div className={styles.card_img}>
                <Image
                  width={800}
                  height={600}
                  src="/services/warehousing-service.jpg"
                  alt=""
                  className="img-fluid"
                />
              </div>
              <h3>
                <Link href="service-details.html">
                  <a className="stretched-link">Warehousing</a>
                </Link>
              </h3>
              <p>
                Quas assumenda non occaecati molestiae. In aut earum sed natus
                eatae in vero. Ab modi quisquam aut nostrum unde et qui est non
                quo nulla
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default OurServices;
