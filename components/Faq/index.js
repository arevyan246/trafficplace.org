import styles from "./Faq.module.css";
import useTranslation from "next-translate/useTranslation";

const Faq = ({ data, language }) => {
  let { t } = useTranslation();
  return (
    <section id="faq" className={styles.faq}>
      <div className="container" data-aos="fade-up">
        <div className="section-header">
          <span>{t('common:faq')}</span>
          <h2>{t('common:faq')}</h2>
        </div>

        <div
          className="row justify-content-center"
          data-aos="fade-up"
          data-aos-delay="200"
        >
          <div className="col-lg-10">
            <div className="accordion accordion-flush" id="faqlist">
              {data &&
                data.map((e) => {
                  return (
                    <div key={e.id} className={styles.accordion_item}>
                      <h3 className="accordion-header">
                        <button
                          className={`${styles.accordion_button} accordion-button collapsed`}
                          type="button"
                          data-bs-toggle="collapse"
                          data-bs-target={`#faq-content-${e.id}`}
                        >
                          <i className="fa-solid fa-question me-2"></i>
                          {e['question_' + language]}
                        </button>
                      </h3>
                      <div
                        id={`faq-content-${e.id}`}
                        className="accordion-collapse collapse"
                        data-bs-parent="#faqlist"
                      >
                        <div
                          className={`${styles.accordion_body} accordion-body`}
                        >
                          {e['answer_' + language]}
                        </div>
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Faq;
