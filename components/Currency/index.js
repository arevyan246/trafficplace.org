import { useState } from "react";

const Currency = () => {
  const [usd, setUsd] = useState();
  const [eur, setEur] = useState();
  const [rub, setRub] = useState();

  const currencyValue = (url, set) => {
    fetch(`https://api.exchangerate.host/convert?from=${url}&to=AMD`)
      .then((x) => x.json())
      .then((y) => set(Number(y.result).toFixed(2)));
  };

  currencyValue("USD", setUsd);
  currencyValue("EUR", setEur);
  currencyValue("RUB", setRub);

  return (
    <>
      <span className="badge bg-warning text-dark rounded-pill me-1">
        USD <span className="fw-bolder">{usd}</span>
      </span>
      <span className="badge bg-warning text-dark rounded-pill me-1">
        EUR <span className="fw-bolder">{eur}</span>
      </span>
      <span className="badge bg-warning text-dark rounded-pill me-1">
        RUB <span className="fw-bolder">{rub}</span>
      </span>
    </>
  );
};

export default Currency;
