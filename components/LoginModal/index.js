import axios from "axios";
import { Formik } from "formik";
import React, { useState } from "react";
import styles from "./LoginModal.module.css";
import { useDispatch, useSelector } from "react-redux";
import { getToken } from "../../features/user/userSlice";
import Link from "next/link";
import Router from "next/router";
import useTranslation from "next-translate/useTranslation";

const LoginModal = () => {
  let { t } = useTranslation();
  const dispatch = useDispatch();
  let loading = useSelector((state) => state.user.loading);
  let errorMessage = useSelector((state) => state.user.errorMessage);

  let submitHandler = (values) => {
    dispatch(getToken(values));
    Router.push("/");
  };
  // const submitHandler = (inputValue) => {
  //   axios
  //     .post("https://cp83762.tmweb.ru/api/login", {
  //       email: inputValue.email,
  //       password: inputValue.password,
  //     })
  //     .then((res) => console.log(res))
  //     .catch((e) => console.log(e.message));
  // };
  return (
    <div
      className="modal fade"
      id="loginModal"
      tabIndex="-1"
      aria-labelledby="loginModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="loginModalLabel">
              {t("common:login")}
            </h5>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="modal-body p-4">
            <Formik
              initialValues={{ email: "", password: "" }}
              validate={(values) => {
                const errors = {};
                if (!values.email) {
                  errors.email = t("common:emailRequired");
                } else if (
                  !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                ) {
                  errors.email = t("common:emailIncomplete");
                }
                if (!values.password) {
                  errors.password = t("common:emailPassword");
                } else if (values.password.length < 8) {
                  errors.password = t("common:emailPasswordV8");
                }
                return errors;
              }}
              onSubmit={(values, { setSubmitting }) => {
                console.log(values, "values");
                submitHandler(values);
                setSubmitting(false);
              }}
            >
              {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                isValid,
                dirty,
              }) => {
                return (
                  <form onSubmit={handleSubmit}>
                    <div className="form-group">
                      <small>
                        <label htmlFor="LoginEmail">
                          {t("common:yourEmail")}
                        </label>
                      </small>
                      <input
                        onChange={handleChange}
                        defaultValue={values.email}
                        name="email"
                        type="email"
                        className="form-control"
                        id="LoginEmail"
                        aria-describedby="emailHelp"
                        placeholder="Email"
                      />
                      <p className="text-danger">
                        {errors.email && touched.email && errors.email}
                      </p>
                    </div>

                    <div className="form-group mt-2">
                      <small>
                        <label htmlFor="LoginPassword">
                          {t("common:yourPassword")}
                        </label>
                      </small>
                      <input
                        onChange={handleChange}
                        defaultValue={values.password}
                        name="password"
                        type="password"
                        className="form-control"
                        id="LoginPassword"
                        placeholder={t("common:yourPassword")}
                      />
                      <p className="text-danger">
                        {errors.password && touched.password && errors.password}
                      </p>
                    </div>
                    <div className="text-end">
                      <button
                        type="submit"
                        className="btn btn-danger"
                        data-bs-dismiss="modal"
                        disabled={!isValid || !dirty}
                      >
                        {t("common:login")}
                        {loading && (
                          <i className="fa-solid fa-spinner fa-spin-pulse"></i>
                        )}
                      </button>
                    </div>

                    <div className="form-group mt-2">
                      <small>
                        <Link href="/forgot-password">
                          <button
                            type="submit"
                            className="btn form-text text-muted"
                            data-bs-dismiss="modal"
                          >
                            {t("common:forgotPassword")}
                          </button>
                        </Link>
                      </small>
                    </div>
                    <p>{errorMessage && errorMessage}</p>
                  </form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginModal;
