import Image from "next/image";
import styles from "./mainSlider.module.css";

const MainSlider = ({ data, language }) => {
  const myLoader = ({ src, width, quality }) => {
    // return `https://cp83762.tmweb.ru/storage/${src}?w=${width}&q=${
      return `https://admin.trafficplace.org/storage/${src}?w=${width}&q=${
      quality || 75
    }`;
  };

  return (
    <div
      id="carouselExampleDark"
      className={`${styles.carousel_container} carousel carousel-dark slide`}
      data-bs-ride="carousel"
    >
      <div className="carousel-indicators">
        <button
          type="button"
          data-bs-target="#carouselExampleDark"
          data-bs-slide-to="0"
          className="active"
          aria-label="Slide 1"
          aria-current="true"
        ></button>
        <button
          type="button"
          data-bs-target="#carouselExampleDark"
          data-bs-slide-to="1"
          aria-label="Slide 2"
          className=""
        ></button>
        <button
          type="button"
          data-bs-target="#carouselExampleDark"
          data-bs-slide-to="2"
          aria-label="Slide 3"
          className=""
        ></button>
      </div>
      <div className="carousel-inner">
        {data &&
          data.map((e) => {
            return (
              <div
                key={e.id}
                className="carousel-item active"
                data-bs-interval="10000"
              >
                <Image
                  width={2500}
                  height={1300}
                  src={e.image}
                  className="d-block w-100"
                  alt={e["text_" + language]}
                  loader={myLoader}
                />
                <div className="carousel-caption d-none d-md-block">
                  <h5 className={styles.slider_title}>
                    {e["text_" + language]}
                  </h5>
                </div>
              </div>
            );
          })}
      </div>
      <button
        className="carousel-control-prev"
        type="button"
        data-bs-target="#carouselExampleDark"
        data-bs-slide="prev"
      >
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Previous</span>
      </button>
      <button
        className="carousel-control-next"
        type="button"
        data-bs-target="#carouselExampleDark"
        data-bs-slide="next"
      >
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="visually-hidden">Next</span>
      </button>
    </div>
  );
};

export default MainSlider;
