import Header from "./Header";
import Footer from "./Footer";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { setToken } from "../features/user/userSlice";
import storage from "../hooks/useStorage";

const Layout = ({ children }) => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setToken(storage().getItem('token')))
  }, []);
  return (
    <>
      <Header />
      {children}
      <Footer />
    </>
  );
};

export default Layout;
