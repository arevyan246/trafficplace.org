import Image from "next/image";
import LoginModal from "../LoginModal";
import RegistrationModal from "../RegistrationModal";
import styles from "./Header.module.css";
import { useDispatch, useSelector } from "react-redux";
import Link from "next/link";
import { logOut } from "../../features/user/userSlice";
import { setLanguage } from "../../features/user/languageSlice";
import apiUrl from "../../configs/api-url";
import { useRouter } from "next/router";
import useTranslation from "next-translate/useTranslation";
import Currency from "../Currency";
import { useEffect, useState } from "react";

const Header = () => {
  let { t } = useTranslation();
  let router = useRouter();
  const dispatch = useDispatch();
  const token = useSelector((state) => state.user.token);
  const language = useSelector((state) => state.language.language);
  const [searchInput,setSearchInput] = useState('')
  let submitHandler = () => {
    dispatch(logOut());
  };

  let addLeng = (lang) => {
    dispatch(setLanguage(lang));
  };
  let searchHandler = ()=>{
      router.push({
        pathname: '/search',
        query: { s: searchInput },
      })
  }
  useEffect(()=>{
    dispatch(setLanguage(router?.locale));
  },[,router])

  return (
    <>
      <header>
        <nav
          className={`${styles.navbar_bg} navbar navbar-expand-lg position-fixed`}
        >
          <div className="container-fluid">
            <Link href="/">
              <a className="navbar-brand text-white">
                <Image
                  className="img-fluid"
                  src={"/logo.png"}
                  width={70}
                  height={70}
                />
              </a>
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <i className="fa-solid fa-bars text-white"></i>
            </button>
            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <div className="d-flex position-relative" role="search">
                    <input
                      className="form-control bg-transparent text-white"
                      type="search"
                      placeholder={t("common:search")}
                      aria-label="Search"
                      onChange={(e)=>setSearchInput(e.target.value)}
                    />
                    <button
                      className="btn btn-outline-light position-absolute end-0"
                      type="submit"
                      onClick={()=>searchHandler()}
                    >
                      <i className="fa-solid fa-magnifying-glass"></i>
                    </button>
                  </div>
                </li>
                <li className="nav-item dropdown">
                  <Link href="/">
                    <a
                      className={`${styles.nav_link} nav-link dropdown-toggle`}
                      role="button"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      {t("common:info")}
                    </a>
                  </Link>
                  <ul className="dropdown-menu">
                    <li>
                      <Link href="/announcements">
                        <a className="dropdown-item">
                          {t("common:announcements")}
                        </a>
                      </Link>
                    </li>
                    <li>
                      <Link href="/news">
                        <a className="dropdown-item">{t("common:news")}</a>
                      </Link>
                    </li>
                    <li>
                      <Link href="/documents">
                        <a className="dropdown-item">{t("common:documents")}</a>
                      </Link>
                    </li>
                  </ul>
                </li>
                <li className="nav-item">
                  <Link href="/about">
                    <a className={`${styles.nav_link} nav-link`}>
                      {t("common:about")}
                    </a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link href="/loads">
                    <a className={`${styles.nav_link} nav-link`}>
                      {t("common:loads")}
                    </a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link href="/cars">
                    <a className={`${styles.nav_link} nav-link`}>
                      {t("common:cars")}
                    </a>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link href="/contact">
                    <a className={`${styles.nav_link} nav-link`}>
                      {t("common:feedback")}
                    </a>
                  </Link>
                </li>
                <li className="nav-item">
                  <span className={`${styles.nav_link} nav-link`}>
                    <Currency />
                  </span>
                </li>
              </ul>
              <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                {!token ? (
                  <>
                    <li className="nav-item m-1 ms-0">
                      <button
                        type="button"
                        className="btn btn-primary"
                        data-bs-toggle="modal"
                        data-bs-target="#loginModal"
                      >
                        {t("common:login")}
                      </button>
                    </li>
                    <li className="nav-item m-1 ms-0">
                      <button
                        type="button"
                        className="btn btn-primary"
                        data-bs-toggle="modal"
                        data-bs-target="#registrationModal"
                      >
                        {t("common:registration")}
                      </button>
                    </li>
                  </>
                ) : (
                  <li className="nav-item dropdown">
                    <Link href="/">
                      <a
                        className={`btn btn-primary m-1 dropdown-toggle`}
                        role="button"
                        data-bs-toggle="dropdown"
                        aria-expanded="false"
                      >
                        {t("common:cabinet")}
                      </a>
                    </Link>
                    <ul className="dropdown-menu">
                      <li>
                        <Link href="/cabinet">
                          <a className="dropdown-item">
                            {t("common:myCabinet")}
                          </a>
                        </Link>
                      </li>
                      <li>
                        <Link href="/loads">
                          <a className="dropdown-item">{t("common:myCrgo")}</a>
                        </Link>
                      </li>
                      <li>
                        <Link href="/cars">
                          <a className="dropdown-item">{t("common:myCars")}</a>
                        </Link>
                      </li>
                      <hr className="m-0" />
                      <li>
                        <button
                          onClick={submitHandler}
                          className="dropdown-item text-danger"
                        >
                          {t("common:out")}
                        </button>
                      </li>
                    </ul>
                  </li>
                )}
                <li className="nav-item dropdown">
                  <button
                    className={`${styles.nav_link} btn nav-link dropdown-toggle`}
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                  >
                    <i className="fa-solid fa-globe me-1"></i>
                  </button>
                  <ul className={`${styles.dropdown_menu} dropdown-menu`}>
                    {router.locales.map((locale) => (
                      <li key={locale}>
                        <Link href={router.asPath} locale={locale}>
                          <button
                            className="dropdown-item"
                            onClick={() => addLeng(locale)}
                          >
                            {locale === "am" ? "ՀԱՅ" : ""}
                            {locale === "en" ? "ENG" : ""}
                            {locale === "ru" ? "РУС" : ""}
                          </button>
                        </Link>
                      </li>
                    ))}
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
      <LoginModal />
      <RegistrationModal />
    </>
  );
};

export default Header;
