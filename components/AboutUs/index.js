import Image from "next/image";
import styles from "./AboutUs.module.css";

const AboutUs = ({ data, language }) => {
  const myLoader = ({ src, width, quality }) => {
    // return `https://cp83762.tmweb.ru/storage/${src}?w=${width}&q=${
      return `https://admin.trafficplace.org/storage/${src}?w=${width}&q=${
      quality || 75
    }`;
  };
  
  return (
    <section id="about" className={`${styles.about} pt-3 mt-5`}>
      <div className="container" data-aos="fade-up">
        <div className="row gy-4">
          <div className="col-lg-6 position-relative align-self-start order-lg-last order-first">
            <Image
              src={data && data[0].image}
              loader={myLoader}
              width={1024}
              height={768}
              className="img-fluid"
              alt={data && data[0].title}
            />
          </div>
          <div
            className={`${styles.content} col-lg-6 order-last  order-lg-first`}
          >
            <h3>{data && data[0]['title_' + language]}</h3>
            <p>{data && data[0]['description_' + language]}</p>
            <ul>
              <li data-aos="fade-up" data-aos-delay="100">
                <i className="bi bi-diagram-3"></i>
                <div>
                  <h5>{data && data[0]['title_one_' + language]}</h5>
                  <p>{data && data[0]['description_one_' + language]}</p>
                </div>
              </li>
              <li data-aos="fade-up" data-aos-delay="200">
                <i className="bi bi-fullscreen-exit"></i>
                <div>
                  <h5>{data && data[0]['title_one_' + language]}</h5>
                  <p>{data && data[0]['description_two_' + language]}</p>
                </div>
              </li>
              <li data-aos="fade-up" data-aos-delay="300">
                <i className="bi bi-broadcast"></i>
                <div>
                  <h5>{data && data[0]['title_three_' + language]}</h5>
                  <p>{data && data[0]['description_three_' + language]}</p>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  );
};

export default AboutUs;
