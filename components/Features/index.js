import Image from "next/image";
import styles from "./Features.module.css";
import useTranslation from "next-translate/useTranslation";

const Features = ({ data, language }) => {
  let { t } = useTranslation();
  const myLoader = ({ src, width, quality }) => {
    // return `https://cp83762.tmweb.ru/storage/${src}?w=${width}&q=${
    return `https://admin.trafficplace.org/storage/${src}?w=${width}&q=${
      quality || 75
    }`;
  };
  console.log(data, "dddd");
  return (
    <section id="features" className={styles.features}>
      <div className="container">
        {data &&
          data.map((e) => {
            return (
              <div
                key={e.id}
                className={`${styles.features_item} row gy-4 align-items-center`}
                data-aos="fade-up"
              >
                <div className={`col-md-5 ${e.id % 2 ? 'order-1 order-md-2' : 'order-2 order-md-1'}`}>
                  <Image
                    src={e.image}
                    loader={myLoader}
                    width={1024}
                    height={768}
                    className="img-fluid"
                    alt={e.title}
                  />
                </div>
                <div className={`col-md-7 ${e.id % 2 ? 'order-2 order-md-1' : 'order-1 order-md-2'}`}>
                  <h3>{e['title_' + language]}</h3>
                  <p className="fst-italic">{e['info_' + language]}</p>
                  <ul>
                    <li>
                      <i className="bi bi-check"></i>{" "}
                      {e['description_' + language]}
                    </li>
                  </ul>
                </div>
              </div>
            );
          })}
      </div>
    </section>
  );
};

export default Features;
